{ config, pkgs, ... }:

{

  system.stateVersion = "21.05";

  #boot.kernelPackages = pkgs.linuxPackages_latest;

  ## Binary Cache for Haskell.nix
  #nix.binaryCachePublicKeys = [
  #  "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
  #  ];
  #  nix.binaryCaches = [
  #  "https://hydra.iohk.io"
  #];

  imports = [ 
    ./hardware-configuration.nix
    ./cachix.nix
  ];

  boot.kernelModules = [
    "thinkpad_acpi"
    #"v4l2loopback device=2"
    #"v4l2loopback exclusive_caps=1 device=2"

    # "coretemp"
  ];

  nix.trustedUsers = [ "julien" "root" ];

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  programs.light.enable = true;
  # programs.singularity.enable = true;

  time.timeZone = "Europe/Paris";

  powerManagement = {
    enable = true;
    powertop.enable = true;
  };

  boot.loader = {
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
  };

  fileSystems."/data" = {
    device = "/dev/disk/by-label/data";
    fsType = "ext4";
  };

  networking = {
    hostName = "espaly"; 
    networkmanager.enable = true;
    # networkmanager.dhcp = "dhcpcd";
    #firewall.allowedTCPPortRanges = [ { from = 3037; to = 3042; } ];
    # interfaces.eth0.ipv4.addresses = [ { address = "192.168.45.107"; prefixLength = 25; } ];
    # defaultGateway = "192.168.45.255";
    # nameservers = [ "193.49.195.10" "195.220.130.2" "193.49.195.20" "195.220.130.10" ];
  };

  console.font = "Lat2-Terminus16";
  console.keyMap = "us";

  i18n = {
    defaultLocale = "en_US.UTF-8";
  };

  environment.systemPackages = with pkgs; [
    cachix
    chromium
    file
    git
    htop
    pavucontrol
    powertop
    tmux
    tree
    vim
    vnstat
    wget

    #linuxPackages.v4l2loopback
    #v4l-utils

  ];

  # nix-direnv
  nix.extraOptions = ''
    keep-outputs = true
    keep-derivations = true
  '';
  #environment.pathsToLink = [
  #  "/share/nix-direnv"
  #];

  services = {

    acpid.enable = true;

    dbus.packages = [ pkgs.gnome3.dconf ];

    #flatpak.enable = true;

    fstrim.enable = true;

    #postgresql = {
    #  enable = true;
    #  authentication = ''
    #    local all all trust
    #  '';
    #};

    printing = {
      enable = true;
      browsing = true;
      clientConf = "ServerName 192.168.45.5";
      #defaultShared = true;
    };

    avahi = {
      enable = true;
      nssmdns = true;
      #publish.userServices = true;
      #publish.enable = true;
    };

    #vnstat.enable = true;

    thinkfan = {
      enable = true;

      # levels = ''
      #   (0,     0,      70)
      #   (2,     65,     75)
      #   (5,     70,     80)
      #   (7,     75,     85)
      #   (127,   80,     32767)
      # '';

      # sensors = ''
      #   hwmon /sys/class/hwmon/hwmon5/temp1_input 
      # '';
    };

    xserver = {
      enable = true;
      layout = "us,fr";
      xkbVariant = ",bepo";
      xkbOptions = "grp:alt_space_toggle";
      libinput.enable = true;
      displayManager.lightdm.enable = true;
      windowManager.i3.enable = true;
      # windowManager.i3.extraPackages = with pkgs; [ dmenu i3status i3lock ];
      wacom.enable = true;
    };

  };

  xdg.portal.enable = true;

  users.users.julien = {
    isNormalUser = true;
    extraGroups = [
      "audio"
      "docker"
      "networkmanager"
      "vboxusers"
      "video"
      "wheel"
    ];
  };

  virtualisation.virtualbox.host.enable = true;
  virtualisation.docker.enable = true;

}

