{ pkgs, ... }:

  {

  home.packages = with pkgs; [
      #breeze-icons
      #gnome3.adwaita-icon-theme
      #gsettings_desktop_schemas
      #glib
      #tev

      baobab
      xfce.mousepad
      #ncdu
      #llvm
      #ghcid

    #hies
    #ghcide865
    vscode
    kickstart

    (haskellPackages.ghcWithPackages (ps: with ps; [
      aeson
      random
      mtl
      text
      transformers
      haskell-gi-base
      hspec
      gi-cairo
      gi-gdk
      gi-glib
      gi-gtk
      gi-cairo-render
      gi-cairo-connector
      QuickCheck
      scotty
    ]))

    #(import (fetchTarball "https://gitlab.com/juliendehos/runcpp/-/archive/master/runcpp-master.tar.gz") {})

    #(import (fetchTarball "https://gitlab.com/juliendehos/autoexam/-/archive/v0.4/autoexam-v0.4.tar.gz") {})
    # (import (fetchTarball "https://gitlab.com/juliendehos/autoexam/-/archive/master/autoexam-master.tar.gz") {})

    #(import (fetchTarball "https://gitlab.com/juliendehos/autoquizer/-/archive/v0.4/autoquizer-v0.4.tar.gz") {})
    # (import (fetchTarball "https://gitlab.com/juliendehos/autoquizer/-/archive/master/autoquizer-master.tar.gz") {})

    audacity
    acpi
    arandr
    aspellDicts.fr

    #bat
    #bc
    #binutils-unwrapped

    cabal-install
    cachix
    #chez
    #cmakeWithGui
    cmake
    #cppcheck
    ctags

    #discord
    doxygen

    #electron
    evince

    ffmpeg

    gcc
    gdb
    geany
    gimp
    gitg
    gnome3.cheese
    gnome3.eog
    #gnome3.gnome-sound-recorder
    gnumake
    gnuplot
    gparted
    guvcview
    glances
    
    heroku
    hlint
    hunspellDicts.fr-any

    imagemagick
    inkscape

    #julia

    klavaro
    killall
    krita

    libreoffice
    librsvg

    meld
    mypaint

    networkmanagerapplet
    nim
    nodejs
    nodePackages.node2nix

    opam
    obs-studio

    pcmanfm
    #python2Packages.doc8    # restructuredtext_lint-1.2.2 not supported for interpreter python3.7

    (python3.withPackages (ps: with ps; [
    #   matplotlib
    #   numpy
    #   pandas
    #   pylint
    #   scikitlearn
    #   scipy

     pygments
     sphinx
    #   sphinx_rtd_theme

    #   (buildPythonPackage rec {
    #     pname = "rstcheck";
    #     version = "3.3.1";
    #     src = pkgs.fetchFromGitHub {
    #       owner = "myint";
    #       repo = "${pname}";
    #       rev = "v${version}";
    #       sha256 = "0wl5mlc7b8sifn5s3c5wv0ga1b99xf7ni6ig186dabpywhv48270";
    #     };
    #     doCheck = false;
    #     propagatedBuildInputs = [ docutils ];
    #   })

    ]))

    #qtcreator
    #xorg.libxcb
    #xorg.xcbutil
    #qt5Full

    ranger
    rustup
    librsvg

    simplescreenrecorder
    slack
    sqlite
    sqlitebrowser
    #stack

    tokei
    texlive.combined.scheme-full
    thunderbird
    # thunderbird-bin
    #teams

    udiskie
    unar
    unzip

    valgrind
    vlc

    xfce.xfce4-screenshooter
    xfce.terminal
    xorg.xkill
    xournal

    youtube-dl

    zlib
    zoom-us

  ];

}

