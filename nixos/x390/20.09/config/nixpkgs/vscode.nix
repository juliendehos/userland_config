{ pkgs, ... }:

{

  programs.vscode = {
    enable = true;

      #haskell = {
      #   enable = true;
      #   hie = {
      #     enable = true;
      #      executablePath = "${pkgs.hies}/bin/hie-wrapper";
      #   };
      #};

      extensions = with pkgs.vscode-extensions; [

        bbenoist.Nix
        ms-vscode.cpptools
        ms-python.python
        #vscodevim.vim

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "nix-env-selector";
            publisher = "arrterian";
            version = "1.0.7";
            sha256 = "sha256:0mralimyzhyp4x9q98x3ck64ifbjqdp8cxcami7clvdvkmf8hxhf";
          };
        })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "cmake-tools";
            publisher = "ms-vscode";
            version = "1.7.3";
            sha256 = "sha256:0jisjyk5n5y59f1lbpbg8kmjdpnp1q2bmhzbc1skq7fa8hj54hp9";
          };
        })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "cmake";
            publisher = "twxs";
            version = "0.0.17";
            sha256 = "sha256:11hzjd0gxkq37689rrr2aszxng5l9fwpgs9nnglq3zhfa1msyn08";
          };
        })

        # (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        #   mktplcRef = {
        #     name = "ghcide";
        #     publisher = "DigitalAssetHoldingsLLC";
        #     version = "0.0.2";
        #     sha256 = "02gla0g11qcgd6sjvkiazzk3fq104b38skqrs6hvxcv2fzvm9zwf";
        #   };
        # })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "vscode-ghc-simple";
            publisher = "dramforever";
            version = "0.2.3";
            sha256 = "sha256:1pd7p4xdvcgmp8m9aymw0ymja1qxvds7ikgm4jil7ffnzl17n6kp";
          };
        })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "language-haskell";
            publisher = "JustusAdam";
            version = "3.4.0";
            sha256 = "sha256:0ab7m5jzxakjxaiwmg0jcck53vnn183589bbxh3iiylkpicrv67y";
          };
        })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "restructuredtext";
            publisher = "lextudio";
            version = "155.0.0";
            sha256 = "sha256:04jqywywxmhpvxz1nxd2hamjd9sg880r4g1wxaz2x4f51z0xda8k";
          };
        })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "todo-tree";
            publisher = "Gruntfuggly";
            version = "0.0.213";
            sha256 = "sha256:0fj7vvaqdldhbzm9dqh2plqlhg34jv5khd690xd87h418sv8rk95";
          };
        })

      ];

      # userSettings = {

      #   # general settings
      #   "window.zoomLevel" = 1.25;
      #   "editor.fontSize" = 13;
      #   "keyboard.dispatch" = "keyCode";
      #   "update.enableWindowsBackgroundUpdates" = false;
      #   "update.mode" = "none";

      #   # # vim
      #   # "vim.easymotion" = true;
      #   # "vim.leader" = ",";

      #   "cmake.configureOnOpen" = false;

      #   #"code-runner.runInTerminal" = true;
      #   
      #   # python
      #   #"python.linting.pylintPath" = "~/.nix-profile/bin/pylint";

      #   # restructuredtext
      #   #"restructuredtext.linter.executablePath" = "~/.nix-profile/bin/doc8";
      #   #"restructuredtext.sphinxBuildPath" = "~/.nix-profile/bin/sphinx-build";
      #   #"restructuredtext.preview.sphinx.disabled" = true;
      #   

      #   # "window.titleBarStyle" = "custom";
      #   #"languageServerHaskell.hieExecutablePath" = "hie-wrapper";
      #   #"languageServerHaskell.hlintOn" = true;
      #   # "languageServerHaskell.enableHIE" = true;
      #   # "languageServerHaskell.trace.server" = "verbose";
      #   # "update.channel" = "none";
      #   # "[nix]"."editor.tabSize" = 2;

      # };

    };

    # home.file.".config/Code/User/keybindings.json".text = ''
    #   [
    #     {
    #         "key": "ctrl+`",
    #         "command": "workbench.action.terminal.focus"
    #     },
    #     {
    #         "key": "ctrl+`",
    #         "command": "workbench.action.focusActiveEditorGroup",
    #         "when": "terminalFocus"
    #     }
    #   ]
    # '';


  }


