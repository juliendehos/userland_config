
{ pkgs, ... }:

{

  imports = [
    ./git.nix
    ./i3status.nix
    ./i3.nix
    ./packages.nix
    ./vim.nix
    ./vscode.nix
    # ./mail.nix
  ];

  home.username = "julien";
  home.homeDirectory = "/home/julien";

  home.file.".stack/config.yaml".text = ''
    nix:
        enable: true
        pure: false
        packages: [zlib]
  '';

  nixpkgs.config.allowUnfree = true;

  home.stateVersion = "20.09";

  home.keyboard = {
    layout = "us(altgr-intl),fr";
    variant = ",bepo";
    options = [
      "grp:alt_shift_toggle"
      "caps:swapescape"
    ];
  };

      
  home.sessionVariables = {
    PS1=''\n\[\w]\$\[\033[0m\]'';
    EDITOR="vim";
    #BROWSER = "firefox";
  };

  gtk = {
    enable = true;
    # iconTheme = {
    #   name = "Adwaita";
    #   package = pkgs.gnome3.adwaita-icon-theme;
    # };
    theme = {
      name = "Shades-of-gray";
      package = pkgs.shades-of-gray-theme;
    };
  };

  qt = {
    enable = true;
    #platformTheme = "gnome";
    platformTheme = "gtk";
  };

  programs = {

    bash = {
      enable = true;
      shellAliases = {
        ll = "ls -lh";
        la = "ls -a";
        vbox = "VirtualBox -style Fusion";
        covideo19 = "covideo19-record covideo19.herokuapp.com 80 1120 480 800 600 25";
        covideo19-1024 = "covideo19-record covideo19.herokuapp.com 80 1920 0 1024 768 25";
      };

      sessionVariables = {
        # TODO PS1 = "\n\[\w]\$\[\033[0m\]";
      };
    };

    dircolors = {
      enable = true;
      settings.DIR = "01;33";
    };

    direnv = {
      enable = true;
      nix-direnv.enable = true;
    };

    home-manager = {
      enable = true;
      #path = "_";
    };

    firefox.enable = true;

    obs-studio = {
      enable = true;
      plugins = [
        #pkgs.obs-linuxbrowser
        pkgs.obs-v4l2sink
      ];
    };

    urxvt = {
      enable = true;
      fonts = ["xft:DejaVu Sans Mono:size=13"];
        extraConfig = {
          reverseVideo = true;
          termName = "xterm-256color";
        };
    };

  };

  services = {

    network-manager-applet.enable = true;

    pasystray.enable = true;

    xscreensaver = {
      enable = true;
      settings = {
         fade = false;
         lock = false;
         # mode = "blank";
         mode = "one";
         selected = 0;
         timeout = "0:05:00";
         programs = "bsod -root -delay 60 -no-windows -no-nt -no-2k -no-win10 -no-msdos -no-amiga -no-glados -no-android -no-apple2 -no-ransomware -no-nvidia -no-os2 -no-mac -no-mac1 -no-vmware -no-macsbug -no-macx -no-os390 -no-vms -no-hvx -no-blitdamage -no-atm -no-sparclinux -no-hppalinux -no-solaris -no-tru64";
      };
    };

    udiskie = {
      enable = true;
      tray = "always";
    };

  };

  # xdg.enable = true;
  # xdg.mime.enable = true;
  # targets.genericLinux.enable=true;

  #xdg.mimeApps.enable = true;
  #xdg.userDirs.enable = true;

  #xdg.userDirs.extraConfig = {
  #  XDG_DATA_DIRS = "${pkgs.gsettings-desktop-schemas}/share/gsettings-schemas/${pkgs.gsettings-desktop-schemas.name}:${pkgs.gtk3}/share/gsettings-schemas/${pkgs.gtk3.name}:$XDG_DATA_DIRS";
  #};

  #xsession.enable = true;
  # xsession.profileExtra = ''
  #   export XDG_DATA_DIRS="/home/julien/.nix-profile/share:$XDG_DATA_DIRS"
  # '';

}

