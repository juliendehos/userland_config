self: super: 

let 
  tpmgr-src = self.fetchFromGitLab {
    owner = "juliendehos";
    repo = "tpmgr";
    rev = "v0.5";
    sha256 = "sha256-j39H+VWZEtRsJHjxcneQAu9tQiP0T3IlShpR+KIbzV4=";
  };

in {
  tpmgr = self.callPackage "${tpmgr-src}/default.nix" {};
}

