{ config, pkgs, ... }:

{
  # Let Home Manager install and manage itself.
  #programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "julien";
  home.homeDirectory = "/home/julien";

  nixpkgs.config.allowUnfree = true;

  #wayland.windowManager.sway = {
  #  enable = true;
  #  wrapperFeatures.gtk = true;
  #};

  imports = [
    ./emacs.nix
    ./git.nix
    ./packages.nix
    ./i3.nix
    ./i3status.nix
    #./sway.nix
    ./vim.nix
    ./nvim.nix
    ./vscode.nix
    #./waybar.nix
  ];


  home.sessionVariables = {
    #PS1=''\n\[\w]\$\[\033[0m\]'';
    EDITOR="vim";
    #BROWSER = "firefox";
  };



  gtk = {
    enable = true;
    iconTheme = {
      name = "Adwaita";
      package = pkgs.gnome3.adwaita-icon-theme;
    };
    theme = {
      name = "Shades-of-gray";
      package = pkgs.shades-of-gray-theme;
    };
  };

  qt = {
    enable = true;
    #platformTheme = "gnome";
    platformTheme = "gtk";
  };


  home.keyboard = {
    layout = "us(altgr-intl)";
    #layout = "us(altgr-intl),fr";
    #variant = ",bepo";
    #options = [
    #  #"grp:alt_shift_toggle"
    #  "caps:swapescape"
    #];
  };

  fonts.fontconfig.enable = true;

  home.packages = [
    (pkgs.nerdfonts.override { 
      fonts = [
        "DejaVuSansMono"
      ]; 
    })
  ];

  programs = {

    bash = {
      enable = true;
    #  shellAliases = {
    #    ll = "ls -lh";
    #    la = "ls -a";
    #    vbox = "VirtualBox -style Fusion";
    #    covideo19 = "covideo19-record covideo19.herokuapp.com 80 1120 480 800 600 25";
    #    covideo19-1024 = "covideo19-record covideo19.herokuapp.com 80 1920 0 1024 768 25";
    #  };

      #sessionVariables = { PS1 = ''\n\[\w]\$\[\033[0m\]''; };
    };

    dircolors = {
      enable = true;
      settings.DIR = "01;33";
    };

    direnv = {
      enable = true;
      nix-direnv.enable = true;
    };

    home-manager = {
      enable = true;
      #path = "_";
    };

    firefox.enable = true;
    
    chromium.enable = true;

    obs-studio = {
      enable = true;
    };

  };


  services = {

    network-manager-applet.enable = true;

    pasystray.enable = true;

    #xscreensaver = {
    #  enable = true;
    #  settings = {
    #     fade = false;
    #     lock = false;
    #     # mode = "blank";
    #     mode = "one";
    #     selected = 0;
    #     timeout = "0:05:00";
    #     programs = "bsod -root -delay 60 -no-windows -no-nt -no-2k -no-win10 -no-msdos -no-amiga -no-glados -no-android -no-apple2 -no-ransomware -no-nvidia -no-os2 -no-mac -no-mac1 -no-vmware -no-macsbug -no-macx -no-os390 -no-vms -no-hvx -no-blitdamage -no-atm -no-sparclinux -no-hppalinux -no-solaris -no-tru64";
    #  };
    #};

    udiskie = {
      enable = true;
      tray = "always";
    };

  };



  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.05";
}
