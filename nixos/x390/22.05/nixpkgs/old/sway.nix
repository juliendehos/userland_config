{ pkgs, config, ... }:

{

  wayland.windowManager.sway = {

    enable = true;

    config = let mod = "Mod4"; in {

      #terminal = "alacritty"; #TODO pkgs ?
      terminal = "xfce4-terminal"; #TODO pkgs ?

      fonts = {
          names = [ "DejaVuSansMono" ];
          # names = [ "DejaVuSansMono" "Terminus" ];
          # style = "Bold Semi-Condensed";
          size = 12.0;
        };

      #bars = [{
      #  fonts = {
      #    names = [ "DejaVuSans" ];
      #    size = 12.0;
      #  };
      #  # TODO statusCommand = "${pkgs.i3status}/bin/i3status"; 
      #}];

      modifier = mod;

      workspaceLayout = "tabbed";

      keybindings = pkgs.lib.mkOptionDefault {

        # TODO "${mod}+m" = "exec ${pkgs.i3lock}/bin/i3lock -n -c 000000";

        "${mod}+c" = "split h";

        "${mod}+h" = "focus left";
        "${mod}+j" = "focus down";
        "${mod}+k" = "focus up";
        "${mod}+l" = "focus right";

        "${mod}+Shift+h" = "move left";
        "${mod}+Shift+j" = "move down";
        "${mod}+Shift+k" = "move up";
        "${mod}+Shift+l" = "move right";

        #"XF86MonBrightnessUp" = "exec light -A 10";
        #"XF86MonBrightnessDown" = "exec light -U 10";

        "XF86MonBrightnessDown" = "exec brightnessctl set 5%-";
        "XF86MonBrightnessUp" = "exec brightnessctl set +5%";

        "XF86AudioRaiseVolume" = "exec pactl set-sink-volume 0 +5%";
        "XF86AudioLowerVolume" = "exec pactl set-sink-volume 0 -5%";
        "XF86AudioMute" = "exec pactl set-sink-mute 0 toggle";
      };

    };

  };

}

