{ pkgs, config, ... }:

{

  programs.waybar = {
    enable = true;

    package = (pkgs.waybar.override (oldAttrs: { pulseSupport = true;} ));

    settings = [{

            layer = "top";
            position = "top";
            height = 24;
            modules-left = ["sway/workspaces" "sway/mode"];
            modules-center = ["sway/window"];
            modules-right = ["custom/stopwatch" "network" "pulseaudio" "battery" "clock" "tray"];
            modules = {
              "sway/workspaces" = {
                format = "{icon}";
                format-icons = {
                  "urgent" = "";
                  "focused" = "";
                  "default" = "";
                };
              };
              "custom/stopwatch" = {
                format = "   {} ";
                exec = "~/.config/waybar/sw";
                on-click = "~/.config/waybar/sw";
                on-click-right = "~/.config/waybar/sw --stop";
                return-type = "json";
              };
              "network" = {
                format-wifi = " {essid} ({signalStrength}%)";
                format-ethernet = " {ifname}: {ipaddr}/{cidr}";
                format-disconnected = "Disconnected ⚠";
              };
              "pulseaudio" = {
                format = "{icon} {volume}%";
                format-bluetooth = "{icon} {volume}%";
                format-muted = " 0%";
                format-icons = {
                  "headphones" = "";
                  "handsfree" = "";
                  "headset" = "";
                  "phone" = "";
                  "portable" = "";
                  "car" = "";
                  "default" = ["" ""];
                };
              };
              "battery" = {
                bat = "BAT0";
                states = {
                  "warning" = 30;
                  "critical" = 15;
                };
                format = "{icon} {capacity}%";
                format-icons = ["" "" "" "" ""];
              };
              "clock" = {
                format = "{:%a %d %b %H:%M}";
              };
            };
          }];


  };

}
 


