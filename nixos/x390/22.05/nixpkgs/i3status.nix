{ pkgs, config, ... }:

{
  
  programs.i3status = {

    enable = true;
    enableDefault = false;

    general = {
      output_format = "i3bar";
      colors = true;
      color_good = "#80d060";
      color_bad = "#d06060";
      interval = 5;
    };

    modules = {

      "wireless wlp0s20f3" = {
        position = 1;
        settings = {
          format_up = " W: (%quality at %essid) %ip ";
          format_down = " W: down ";
        };
      };

      "ethernet enp0s31f6" = {
        position = 2;
        settings = {
          format_up = " E1: %ip (%speed) ";
          format_down = " E1: down ";
        };
      };

      "battery 0" = {
        position = 3;
        settings = {
          format = " B: %status %percentage %remaining (%consumption) ";
          low_threshold = 10;
          threshold_type = "percentage";
        };
      };

      "volume master" = {
        position = 4;
        settings = {
          format = " ♪: %volume ";
          device = "pulse:0";
          # device = "default"
          # mixer = "Master"
          # mixer_idx = 0
        };
      };

      "load" = {
        position = 5;
        settings = {
          format = " L: %5min ";
        };
      };

      "cpu_usage" = {
        position = 6;
        settings = {
          format = " CPU: %usage ";
        };
      };

      "cpu_temperature 0" = {
        position = 7;
        settings = {
          format = " T: %degrees C";
          max_threshold = 75;
          path = "/sys/devices/platform/coretemp.0/hwmon/hwmon7/temp1_input";
        };
      };

      "disk /" = {
        position = 8;
        settings = {
          format = " /: %avail ";
        };
      };

      "tztime local" = {
        position = 9;
        settings = {
          format = " %Y-%m-%d %H:%M ";
        };
      };

      #"volume master" = {
      #  position = 1;
      #  settings = {
      #    format = "♪ %volume";
      #    format_muted = "♪ muted (%volume)";
      #    device = "pulse:1";
      #  };
      #};

      #"disk /" = {
      #  position = 2;
      #  settings = {
      #    format = "/ %avail";
      #  };
      #};

    };

  };

}

