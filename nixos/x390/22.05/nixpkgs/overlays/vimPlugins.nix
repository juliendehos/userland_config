self: super: {

  vimPlugins = super.vimPlugins // {

    minibufexpl = self.vimUtils.buildVimPluginFrom2Nix { 
      name = "minibufexpl.vim-2013-06-16";
      src = self.fetchgit {
        url = "https://github.com/fholgado/minibufexpl.vim";
        rev = "ad72976ca3df4585d49aa296799f14f3b34cf953";
        sha256 = "1bfq8mnjyw43dzav8v1wcm4rrr2ms38vq8pa290ig06247w7s7ng";
      };
      dependencies = [];
    };

    badwolf = self.vimUtils.buildVimPluginFrom2Nix { 
        name = "badwolf";
        src = self.fetchgit {
            url = "https://github.com/sjl/badwolf";
            rev = "v1.6.0";
            sha256 = "1hywj43ww2wz0z0mrhvlzkifrvswnwgbnnr0hbrg8vs5d131ihg6";
        };
        dependencies = [];
    };

    nvim-tree = self.vimUtils.buildVimPluginFrom2Nix { 
        name = "nvim-tree";
        src = self.fetchgit {
            url = "https://github.com/nvim-tree/nvim-tree.lua";
            rev = "c49499413a875fc99ce8594cfe6474ed684e51eb";
            sha256 = "sha256-E0C3Jz0Xk8Si/mzmS2NUMd9Y9QpOGw1BqvREP7dHrFo=";
        };
        dependencies = [];
    };

    bufferline = self.vimUtils.buildVimPluginFrom2Nix { 
        name = "bufferline";
        src = self.fetchgit {
            url = "https://github.com/akinsho/bufferline.nvim";
            rev = "v3.1.0";
            sha256 = "sha256-Gb3fHut/wBPCtgp916FOQ0x1D5kijj7iZhYe7AlpQRM=";
        };
        dependencies = [];
    };

    scope = self.vimUtils.buildVimPluginFrom2Nix { 
        name = "scope";
        src = self.fetchgit {
            url = "https://github.com/tiagovla/scope.nvim";
            rev = "2db6d31de8e3a98d2b41c0f0d1f5dc299ee76875";
            sha256 = "sha256-BdX+C5KBHZcFgDG2fXvhMl2Gp/6ffJWEBvA/JvVWh4I=";
        };
        dependencies = [];
    };

    hop = self.vimUtils.buildVimPluginFrom2Nix { 
        name = "hop";
        src = self.fetchgit {
            url = "https://github.com/phaazon/hop.nvim";
            rev = "v2.0.3";
            sha256 = "sha256-UZZlo5n1x8UfM9OP7RHfT3sFRfMpLkBLbEdcSO+SU6E=";
        };
        dependencies = [];
    };

    lualine = self.vimUtils.buildVimPluginFrom2Nix { 
        name = "lualine";
        src = self.fetchgit {
            url = "https://github.com/nvim-lualine/lualine.nvim";
            rev = "3325d5d43a7a2bc9baeef2b7e58e1d915278beaf";
            sha256 = "sha256-pjpxV59Ej7tCvReP5nFi/h1veGwzLdw3PILC89+f488=";
        };
        dependencies = [];
    };

    feline = self.vimUtils.buildVimPluginFrom2Nix { 
        name = "feline";
        src = self.fetchgit {
            url = "https://github.com/feline-nvim/feline.nvim";
            rev = "v1.1.3";
            sha256 = "sha256-c6wuGIKcWH8q9d5Hl9Gimx2raqspqvUigVpRSAwJcus=";
        };
        dependencies = [];
    };

    devicons = self.vimUtils.buildVimPluginFrom2Nix { 
        name = "devicons";
        src = self.fetchgit {
            url = "https://github.com/nvim-tree/nvim-web-devicons";
            rev = "3b1b794bc17b7ac3df3ae471f1c18f18d1a0f958";
            sha256 = "sha256-hxujmUwNtDAXd6JCxBpvPpOzEENQSOYepS7fwmbZufs=";
        };
        dependencies = [];
    };

    gitsigns = self.vimUtils.buildVimPluginFrom2Nix { 
        name = "gitsigns";
        src = self.fetchgit {
            url = "https://github.com/lewis6991/gitsigns.nvim";
            rev = "v0.5";
            sha256 = "sha256-kyiQoboYq4iNLOj1iKA2cfXQ9FFiRYdvf55bX5Xvj8A=";
        };
        dependencies = [];
    };

    toggleterm = self.vimUtils.buildVimPluginFrom2Nix { 
        name = "toggleterm";
        src = self.fetchgit {
            url = "https://github.com/akinsho/toggleterm.nvim";
            rev = "2.3.0";
            sha256 = "sha256-AmE4hE0SNZUSZ4A92Wuc6Rb4GZY+znVRIpXR4k8sywc=";
        };
        dependencies = [];
    };

  };

}

