{ pkgs, ... }:

{

  programs.vim = {

    enable = true;

    plugins = with pkgs.vimPlugins; [
      agda-vim
      airline
      ale
      badwolf
      easymotion
      idris2-vim
      julia-vim
      minibufexpl
      nerdtree
      nim-vim
      rust-vim
      tagbar
      vim-colorschemes
      vim-nix
    ];

    extraConfig = builtins.readFile ./vimrc;
  };

}

