self: super: {

  kickstart = self.rustPlatform.buildRustPackage rec {
    pname = "kickstart";
    version = "v0.5.0";

    src = self.fetchFromGitHub {
      owner = "Keats";
      repo = pname;
      rev = version;
           sha256 = "sha256-4POxv6fIrp+wKb9V+6Y2YPx3FXp3hpnkq+62H9TwGII=";
    };

    doCheck = false;
    cargoSha256 = "sha256-vDlIrAQsmTa3XwP+w4r7alyfCAkGLEpVUcOuJOo/tt0=";

  };

}

