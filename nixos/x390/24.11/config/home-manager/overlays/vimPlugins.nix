self: super: {

  vimPlugins = super.vimPlugins // {

    minibufexpl = self.vimUtils.buildVimPlugin { 
      name = "minibufexpl.vim-2013-06-16";
      src = self.fetchgit {
        url = "https://github.com/fholgado/minibufexpl.vim";
        rev = "ad72976ca3df4585d49aa296799f14f3b34cf953";
        sha256 = "1bfq8mnjyw43dzav8v1wcm4rrr2ms38vq8pa290ig06247w7s7ng";
      };
      dependencies = [];
    };

    badwolf = self.vimUtils.buildVimPlugin { 
        name = "badwolf";
        src = self.fetchgit {
            url = "https://github.com/sjl/badwolf";
            rev = "v1.6.0";
            sha256 = "1hywj43ww2wz0z0mrhvlzkifrvswnwgbnnr0hbrg8vs5d131ihg6";
        };
        dependencies = [];
    };

    nvim-tree = self.vimUtils.buildVimPlugin { 
        name = "nvim-tree";
        src = self.fetchgit {
            url = "https://github.com/nvim-tree/nvim-tree.lua";
            rev = "nvim-tree-v1.10.0";
           sha256 = "sha256-33spbOlZaHYuFxY6DHSlg/RyMb1lC4TCM/KrpacUp0A=";
      #     sha256 = "sha256-wundHVZ0GNcddIh1Af+fBobdKsswk+MMoFVnI7hjgTQ=";
        };
        dependencies = [];
    };

    bufferline = self.vimUtils.buildVimPlugin { 
        name = "bufferline";
        src = self.fetchgit {
            url = "https://github.com/akinsho/bufferline.nvim";
            rev = "v4.9.1";
           sha256 = "sha256-ae4MB6+6v3awvfSUWlau9ASJ147ZpwuX1fvJdfMwo1Q=";
      #     sha256 = "sha256-wundHVZ0GNcddIh1Af+fBobdKsswk+MMoFVnI7hjgTQ=";
        };
        dependencies = [];
    };

    scope = self.vimUtils.buildVimPlugin { 
        name = "scope";
        src = self.fetchgit {
            url = "https://github.com/tiagovla/scope.nvim";
            rev = "e1799fa";
           sha256 = "sha256-rOuEDd90dMRCa+zikTQdE+D4rpx4PFmsk8JpvjK1b7E=";
      #     sha256 = "sha256-wundHVZ0GNcddIh1Af+fBobdKsswk+MMoFVnI7hjgTQ=";
        };
        dependencies = [];
    };

    hop = self.vimUtils.buildVimPlugin { 
        name = "hop";
        src = self.fetchgit {
            url = "https://github.com/phaazon/hop.nvim";
            rev = "v2.0.3";
            sha256 = "sha256-UZZlo5n1x8UfM9OP7RHfT3sFRfMpLkBLbEdcSO+SU6E=";
        };
        dependencies = [];
    };

    lualine = self.vimUtils.buildVimPlugin { 
        name = "lualine";
        src = self.fetchgit {
            url = "https://github.com/nvim-lualine/lualine.nvim";
            rev = "2a5bae9";
           sha256 = "sha256-IN6Qz3jGxUcylYiRTyd8j6me3pAoqJsJXtFUvph/6EI=";
      #     sha256 = "sha256-wundHVZ0GNcddIh1Af+fBobdKsswk+MMoFVnI7hjgTQ=";
        };
        dependencies = [];
    };

    devicons = self.vimUtils.buildVimPlugin { 
        name = "devicons";
        src = self.fetchgit {
            url = "https://github.com/nvim-tree/nvim-web-devicons";
            rev = "v0.100";
            sha256 = "sha256-DSUTxUFCesXuaJjrDNvurILUt1IrO5MI5ukbZ8D87zQ=";
        };
        dependencies = [];
    };

    gitsigns = self.vimUtils.buildVimPlugin { 
        name = "gitsigns";
        src = self.fetchgit {
            url = "https://github.com/lewis6991/gitsigns.nvim";
            rev = "v1.0.0";
           sha256 = "sha256-G9Gv4xH77yS+xvkw9IWbb39b3egWan9ZIiReooBArvQ=";
      #     sha256 = "sha256-wundHVZ0GNcddIh1Af+fBobdKsswk+MMoFVnI7hjgTQ=";
        };
        dependencies = [];
    };

    toggleterm = self.vimUtils.buildVimPlugin { 
        name = "toggleterm";
        src = self.fetchgit {
            url = "https://github.com/akinsho/toggleterm.nvim";
            rev = "v2.13.1";
           sha256 = "sha256-Xc3TZUA6glsuchignUSk4gLZs1IBvI+YnWeP1r+snbQ=";
      #     sha256 = "sha256-wundHVZ0GNcddIh1Af+fBobdKsswk+MMoFVnI7hjgTQ=";
        };
        dependencies = [];
    };

    lean = self.vimUtils.buildVimPlugin { 
        name = "lean";
        src = self.fetchgit {
            url = "https://github.com/Julian/lean.nvim";
            rev = "v2024.12.2";
           sha256 = "sha256-Kz5xpp+HOQtTps5c+7mFW2oLButgCnLDALceDkhp9mo=";
      #     sha256 = "sha256-wundHVZ0GNcddIh1Af+fBobdKsswk+MMoFVnI7hjgTQ=";
        };
        dependencies = [];
    };

  };

}

