{ pkgs, ... }:

{

  programs.emacs = {
    enable = true;

    extraPackages = (epkgs: with epkgs; [
      agda2-mode
      gruvbox-theme
    ] );

  };
}


