{ pkgs, ... }:

{

  programs.neovim = {

    enable = true;

    plugins = with pkgs.vimPlugins; [

      ale
      badwolf
      hop
      nvim-tree
      vim-nix

      bufferline
      scope

      lualine
      devicons
      gitsigns

      toggleterm

      idris2-nvim
      nvim-lspconfig
      nui-nvim

      #agda-vim
      lean
      # rust-vim

    ];

    extraConfig = builtins.readFile ./nvimrc;

  };

}

# https://github.com/sindrets/diffview.nvim
# https://github.com/tanvirtin/vgit.nvim

# https://neovim.io/doc/user/lsp.html#vim.lsp.start()
# https://github.com/ms-jpq/coq_nvim
# https://haskell4nix.readthedocs.io/nixpkgs-users-guide.html#how-to-install-haskell-language-server
# https://github.com/dense-analysis/ale#usage-completion

