{ pkgs, config, ... }:

{

  xsession.windowManager.i3 = {

    enable = true;

    extraConfig = "exec i3-msg workspace 1";

    config = let mod = "Mod4"; in {

      #terminal = "${pkgs.roxterm}/bin/roxterm";
      terminal = "${pkgs.xfce.xfce4-terminal}/bin/xfce4-terminal";
      #terminal = "${pkgs.lxterminal}/bin/lxterminal";

      fonts = {
          names = [ "DejaVuSans" ];
          # style = "Bold Semi-Condensed";
          size = 12.0;
        };

      bars = [{
        fonts = {
          names = [ "DejaVuSans" ];
          size = 12.0;
        };
        statusCommand = "${pkgs.i3status}/bin/i3status"; 
      }];

      modifier = mod;

      workspaceLayout = "tabbed";

      keybindings = pkgs.lib.mkOptionDefault {

        "${mod}+m" = "exec ${pkgs.i3lock}/bin/i3lock -n -c 000000";

        "${mod}+Shift+e" = "exec ~/.config/home-manager/i3_exit_menu.sh";

        "${mod}+c" = "split h";

        "${mod}+h" = "focus left";
        "${mod}+j" = "focus down";
        "${mod}+k" = "focus up";
        "${mod}+l" = "focus right";

        "${mod}+Shift+h" = "move left";
        "${mod}+Shift+j" = "move down";
        "${mod}+Shift+k" = "move up";
        "${mod}+Shift+l" = "move right";

        #"XF86MonBrightnessUp" = "exec light -A 10";
        #"XF86MonBrightnessDown" = "exec light -U 10";

        "XF86MonBrightnessDown" = "exec brightnessctl set 5%-";
        "XF86MonBrightnessUp" = "exec brightnessctl set +5%";

        "XF86AudioRaiseVolume" = "exec pactl set-sink-volume 0 +5%";
        "XF86AudioLowerVolume" = "exec pactl set-sink-volume 0 -5%";
        "XF86AudioMute" = "exec pactl set-sink-mute 0 toggle";
      };

    };

  };

}

