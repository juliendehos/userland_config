{ pkgs, ... }:

{

  programs.vscode = {
    enable = true;

    extensions = with pkgs.vscode-extensions; [

      bbenoist.nix
      arrterian.nix-env-selector
      mhutchie.git-graph
      gruntfuggly.todo-tree
      # vscodevim.vim

      ms-vscode.cpptools
      ms-vscode.cmake-tools
      twxs.cmake

      ms-python.python

      banacorn.agda-mode

      # haskell.haskell
      justusadam.language-haskell

      rust-lang.rust-analyzer

      # ocamllabs.ocaml-platform

      # (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
      #   mktplcRef = {
      #     name = "vscode-ghc-simple";
      #     publisher = "dramforever";
      #     version = "0.2.3";
      #     sha256 = "sha256:1pd7p4xdvcgmp8m9aymw0ymja1qxvds7ikgm4jil7ffnzl17n6kp";
      #   };
      # })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "lean4";
          publisher = "leanprover";
          version = "0.0.159";
          sha256 = "sha256-P7Fe7HVpFB79Y9QtHrAS2sdnHTa3cv5uxSmHFiKfXMY";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "idris2-lsp";
          publisher = "bamboo";
          version = "0.7.0";
          sha256 = "sha256-8eLvHKUPBoge50wzOfp5aK/XVJElVzKtil8Yj+PwNUU=";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "vscode-ghc-simple";
          publisher = "dramforever";
          version = "0.2.4";
          sha256 = "sha256-CMECeHvUCCpJG2gUk9FvbUF8T2+04ngWDLgPMsR1FjQ=";
        };
      })

    ];
  };


      #   # general settings
      #   "window.zoomLevel" = 1.25;
      #   "editor.fontSize" = 13;
      #   "keyboard.dispatch" = "keyCode";
      #   "update.enableWindowsBackgroundUpdates" = false;
      #   "update.mode" = "none";

      #   # # vim
      #   # "vim.easymotion" = true;
      #   # "vim.leader" = ",";

      #   "cmake.configureOnOpen" = false;

      #   #"code-runner.runInTerminal" = true;
      #   
      #   # python
      #   #"python.linting.pylintPath" = "~/.nix-profile/bin/pylint";

      #   # restructuredtext
      #   #"restructuredtext.linter.executablePath" = "~/.nix-profile/bin/doc8";
      #   #"restructuredtext.sphinxBuildPath" = "~/.nix-profile/bin/sphinx-build";
      #   #"restructuredtext.preview.sphinx.disabled" = true;
      #   

      #   # "window.titleBarStyle" = "custom";
      #   #"languageServerHaskell.hieExecutablePath" = "hie-wrapper";
      #   #"languageServerHaskell.hlintOn" = true;
      #   # "languageServerHaskell.enableHIE" = true;
      #   # "languageServerHaskell.trace.server" = "verbose";
      #   # "update.channel" = "none";
      #   # "[nix]"."editor.tabSize" = 2;

      # };

    # home.file.".config/Code/User/keybindings.json".text = ''
    #   [
    #     {
    #         "key": "ctrl+`",
    #         "command": "workbench.action.terminal.focus"
    #     },
    #     {
    #         "key": "ctrl+`",
    #         "command": "workbench.action.focusActiveEditorGroup",
    #         "when": "terminalFocus"
    #     }
    #   ]
    # '';


  }


