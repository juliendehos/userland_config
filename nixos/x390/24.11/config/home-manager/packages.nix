{ pkgs, ... }:

  {

  home.packages = with pkgs; [

    xsel
    xclip

    elan

    anki
    neovide
    tor-browser-bundle-bin
    #zotero
    #obsidian

    coqPackages.coqide
    coq
    exercism
    ocaml
    dune_2
    stack
    idris2
    #agda
    (agda.withPackages (ps: [ ps.standard-library ]))


    tpmgr

    #termonad
    lxterminal
    roxterm
    xfce.xfce4-terminal
      alacritty

      ripgrep
      appimage-run

      #foot
      #swaylock
      #swayidle
      #wl-clipboard
      #wf-recorder
      #mako
      ##dmenu
      #xwayland

      #sway-contrib.grimshot
      #wdisplays
      #xdg-desktop-portal-wlr
      #xdg-desktop-portal-gtk
      #xdg-desktop-portal

      xfce.xfce4-screenshooter
      simplescreenrecorder

      baobab
      xfce.mousepad
      #ncdu
      #llvm
      #ghcid

    vscode
    kickstart

    haskell-language-server
    (haskellPackages.ghcWithPackages (ps: with ps; [
      aeson
      random
      mtl
      text
      transformers
      hspec
    ]))

    #(import (fetchTarball "https://gitlab.com/juliendehos/runcpp/-/archive/master/runcpp-master.tar.gz") {})

    #(import (fetchTarball "https://gitlab.com/juliendehos/autoexam/-/archive/v0.4/autoexam-v0.4.tar.gz") {})
    # (import (fetchTarball "https://gitlab.com/juliendehos/autoexam/-/archive/master/autoexam-master.tar.gz") {})

    #(import (fetchTarball "https://gitlab.com/juliendehos/autoquizer/-/archive/v0.4/autoquizer-v0.4.tar.gz") {})
    # (import (fetchTarball "https://gitlab.com/juliendehos/autoquizer/-/archive/master/autoquizer-master.tar.gz") {})

    audacity
    #acpi
    arandr
    aspellDicts.fr

    #bat
    bc
    #binutils-unwrapped

    cabal-install
    chez
    cmake
    #cppcheck
    ctags

    doxygen

    evince
    eog

    ffmpeg

    gcc
    gdb
    gimp
    gitg
    gnumake
    gnuplot
    gparted
    guvcview
    glances
    
    #heroku
    hlint
    hunspellDicts.fr-any

    imagemagick
    inkscape

    klavaro
    killall
    krita

    libreoffice
    librsvg

    mdbook
    meld
    mypaint

    #networkmanagerapplet
    nim
    #nerdfonts
    #nodejs
    #nodePackages.node2nix

    opam
    #obs-studio

    pcmanfm

    #python2Packages.doc8    # restructuredtext_lint-1.2.2 not supported for interpreter python3.7

    (python3.withPackages (ps: with ps; [

      # pip
      pygments
      # sphinx
      # sphinx_rtd_theme
      # rstcheck

    #   matplotlib
    #   numpy
    #   pandas
    #   pylint
    #   scikitlearn
    #   scipy

    #   (buildPythonPackage rec {
    #     pname = "rstcheck";
    #     version = "3.3.1";
    #     src = pkgs.fetchFromGitHub {
    #       owner = "myint";
    #       repo = "${pname}";
    #       rev = "v${version}";
    #       sha256 = "0wl5mlc7b8sifn5s3c5wv0ga1b99xf7ni6ig186dabpywhv48270";
    #     };
    #     doCheck = false;
    #     propagatedBuildInputs = [ docutils ];
    #   })

    ]))

    xorg.libxcb
    xorg.xcbutil

    ranger
    rustup
    librsvg

    sqlite
    sqlitebrowser
    #stack

    #teams
    tokei
    texlive.combined.scheme-full
    thunderbird

    #udiskie
    unar
    unzip

    valgrind
    vlc

    pulseaudio
    xorg.xkill
    xournal

    #youtube-dl
    yt-dlp

    zlib
    # zoom-us

  ];

}

