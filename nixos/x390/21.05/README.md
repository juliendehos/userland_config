
```
nix-channel --add https://github.com/rycee/home-manager/archive/release-20.05.tar.gz home-manager
nix-channel --update

# logout/login

nix-shell '<home-manager>' -A install
mkdir ~/.config
cp -r nixpkgs ~/.config/
home-manager switch
```

