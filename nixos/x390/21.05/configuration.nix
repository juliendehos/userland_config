# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  nix.trustedUsers = [ "julien" "root" ];

  #boot.kernelModules = [
  #  "thinkpad_acpi"
  #];

  programs.dconf.enable = true;

  networking = {
    hostName = "x390"; 
    networkmanager.enable = true;
  };

  time.timeZone = "Europe/Paris";

  networking.useDHCP = false;
  #networking.interfaces.enp0s31f6.useDHCP = true;
  #networking.interfaces.wlp0s20f3.useDHCP = true;

  i18n.defaultLocale = "fr_FR.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  services = {
    #getty.autologinUser = "julien";  

    #acpid.enable = true;

    dbus.packages = [ pkgs.gnome3.dconf ];

    #flatpak.enable = true;

    fstrim.enable = true;

    #postgresql = {
    #  enable = true;
    #  authentication = ''
    #    local all all trust
    #  '';
    #};

    printing = {
      enable = true;
      browsing = true;
      clientConf = "ServerName 192.168.45.5";
      #defaultShared = true;
    };

    interception-tools = {
      enable = true;
      plugins = [ pkgs.interception-tools-plugins.caps2esc ];
    };

    avahi = {
      enable = true;
      nssmdns = true;
      #publish.userServices = true;
      #publish.enable = true;
    };

    #thinkfan = {
    #  enable = true;

      # levels = ''
      #   (0,     0,      70)
      #   (2,     65,     75)
      #   (5,     70,     80)
      #   (7,     75,     85)
      #   (127,   80,     32767)
      # '';
   
      # sensors = ''
      #   hwmon /sys/class/hwmon/hwmon5/temp1_input
      # '';
    #};

    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      #jack.enable = true;

      # use the example session manager (no others are packaged yet so this is enabled by default,
      # no need to redefine it in your config for now)
      #media-session.enable = true;
    };

    xserver = {
      enable = true;
      layout = "us";
      #xkbVariant = ",bepo";
      #xkbOptions = "grp:alt_space_toggle";
      libinput.enable = true;
      displayManager.autoLogin.enable = true;
      displayManager.autoLogin.user = "julien";
      displayManager.lightdm.enable = true;
      windowManager.i3.enable = true;
      # windowManager.i3.extraPackages = with pkgs; [ dmenu i3status i3lock ];
      wacom.enable = true;
    };

  };

  xdg.portal.enable = true;

  #hardware.opengl = {
  #  enable = true;
  #  driSupport = true;
  #};

  security.rtkit.enable = true; # for pipewire

  powerManagement = {
    enable = true;
    powertop.enable = true;
  };

  users.users.julien = {
    isNormalUser = true;
    extraGroups = [ 
      "wheel" 
      "docker"
      "networkmanager"
      "vboxusers"
      "video"
     ];
  };

  environment.systemPackages = with pkgs; [
    alsaUtils
    brightnessctl
    cachix
    file
    git
    htop
    pavucontrol
    powertop
    tmux
    tree
    vim
    wget
  ];

  # nix-direnv
  nix.extraOptions = ''
    keep-outputs = true
    keep-derivations = true
  '';
  environment.pathsToLink = [
    "/share/nix-direnv"
  ];

  virtualisation.virtualbox.host.enable = true;
  virtualisation.docker.enable = true;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # networking.firewall.enable = false;
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

}

