self: super: {

  vimPlugins = super.vimPlugins // {

    minibufexpl = self.vimUtils.buildVimPluginFrom2Nix { 
      name = "minibufexpl.vim-2013-06-16";
      src = self.fetchgit {
        url = "https://github.com/fholgado/minibufexpl.vim";
        rev = "ad72976ca3df4585d49aa296799f14f3b34cf953";
        sha256 = "1bfq8mnjyw43dzav8v1wcm4rrr2ms38vq8pa290ig06247w7s7ng";
      };
      dependencies = [];
    };

    badwolf = self.vimUtils.buildVimPluginFrom2Nix { 
        name = "badwolf";
        src = self.fetchgit {
            url = "https://github.com/sjl/badwolf";
            rev = "v1.6.0";
            sha256 = "1hywj43ww2wz0z0mrhvlzkifrvswnwgbnnr0hbrg8vs5d131ihg6";
        };
        dependencies = [];
    };

  };

}

