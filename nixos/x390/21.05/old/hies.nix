
self: super: rec {
  all-hies = import (fetchTarball "https://github.com/infinisil/all-hies/tarball/master") {};
  hies = all-hies.selection { selector = p: { inherit (p) ghc865 ghc844; }; };
}

