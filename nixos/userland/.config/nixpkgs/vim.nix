{pkgs}:

let

    _minibufexpl = pkgs.vimUtils.buildVimPluginFrom2Nix { 
        name = "minibufexpl.vim-2013-06-16";
        src = pkgs.fetchgit {
            url = "https://github.com/fholgado/minibufexpl.vim";
            rev = "ad72976ca3df4585d49aa296799f14f3b34cf953";
            sha256 = "1bfq8mnjyw43dzav8v1wcm4rrr2ms38vq8pa290ig06247w7s7ng";
        };
        dependencies = [];
    };

in

pkgs.vim_configurable.customize {

    name = "vim";

    vimrcConfig.customRC = ''
        " Use Vim settings, rather then Vi settings. 
        " This setting must be as early as possible, as it has side effects.
        set nocompatible
        set nofoldenable

        " Leader
        let mapleader = ","

        " display options
        set t_Co=256
        set gfn=Monospace\ 13
        syntax on
        colorscheme molokai_dark
        "colorscheme burnttoast256

        set number
        set cursorline
        set colorcolumn=80
        set ruler         " show the cursor position all the time
        set showcmd       " display incomplete command

        " search
        set hlsearch
        set incsearch     " do incremental searching
        set ignorecase    " case insensitive searching (unless specified)
        set smartcase

        " backup and undo
        set undofile
        set history=100

        " completion
        set wildmode=longest:full,full
        set wildmenu

        " mouse and scrolling
        set mouse=a
        set scrolloff=8 

        " tabulation
        set tabstop=4
        set expandtab
        set shiftwidth=4

        " Open new split panes to right and bottom, which feels more natural
        set splitbelow
        set splitright

        " HTML Editing
        set matchpairs+=<:>

        " Quicker window movement
        nnoremap <C-j> <C-w>j
        nnoremap <C-k> <C-w>k
        nnoremap <C-h> <C-w>h
        nnoremap <C-l> <C-w>l

        " Always use vertical diffs
        set diffopt+=vertical

        " resize panes
        nnoremap <silent> <Right> :vertical resize +5<cr>
        nnoremap <silent> <Left> :vertical resize -5<cr>
        nnoremap <silent> <Up> :resize +5<cr>
        nnoremap <silent> <Down> :resize -5<cr>

        " navigate through compilation errors
        nmap <F4> :cp<cr>
        nmap <F5> :cn<cr>

        " spell checking
        map <silent> <F10> "<Esc>:silent setlocal spell! spelllang=fr<CR>"
        map <silent> <F11> "<Esc>:silent setlocal spell! spelllang=en<CR>"
        map <silent> <F12> "<Esc>:silent setlocal spell! spelllang=en,fr<CR>"
        nnoremap <F1> z=

        " delete buffer without losing the split window
        nnoremap <C-c> :bp\|bd #<CR>

        " ---- plugins ----- "

        " lightline
        set laststatus=2

        " nerdtree
        nmap <F6> :NERDTreeToggle<CR>

        " tagbar
        nmap <F7> :TagbarToggle<CR>

        " minibufexpl
        let g:miniBufExplorerAutoStart = 1
        nmap <F2> :MBEbp<cr>
        nmap <F3> :MBEbn<cr>

        let g:tagbar_type_julia = {
          \ 'ctagstype' : 'julia',
          \ 'kinds'     : [
              \ 't:struct', 'f:function', 'm:macro', 'c:const']
          \ }

        " ale error navigation
        nmap <F8> <Plug>(ale_previous_wrap)
        nmap <F9> <Plug>(ale_next_wrap)

        let g:ale_linters = {'haskell': ['hlint', 'ghc']}
        let g:ale_haskell_ghc_options = '-fno-code -v0 -isrc -Wall'

    '';

    vimrcConfig.packages.myVimPackage = with pkgs.vimPlugins; {
        start = [ 
            ale
            easymotion
            julia-vim
            lightline-vim
            _minibufexpl
            nerdtree
            tagbar
            vim-colorschemes
            vim-nix
        ];
    };

}

