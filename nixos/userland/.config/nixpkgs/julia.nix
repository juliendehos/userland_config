
{ stdenv, makeWrapper, fetchurl }:

let
  version = "1.1.0";
  sha256 = "0walq0hay9cihhhkyaksvgqmkd4vv2pj0j95qdg19d96wl9x1kw0";
  _major = "1.1";
  _pname = "julia-${version}";
in

stdenv.mkDerivation rec {
  inherit version;
  name = "julia-bin-${version}";

  src = fetchurl {
    url = "https://julialang-s3.julialang.org/bin/linux/x64/${_major}/${_pname}-linux-x86_64.tar.gz";
    inherit sha256;
  };

  buildPhase = ":";

  installPhase = ''
    mkdir -p $out
    tar zxf $src -C $out
    mv $out/${_pname}/* $out/
    rmdir $out/${_pname}
  '';

  preFixup = ''
    patchelf --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" $out/bin/julia
  '';
}

