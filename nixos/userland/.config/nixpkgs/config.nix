with (import <nixos> {});

let

  _pkgs1809 = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/18.09.tar.gz") {};

  _vim = import ./vim.nix { inherit pkgs; };

  _julia = callPackage ./julia.nix {};

  _hie = (import (fetchTarball "https://github.com/domenkozar/hie-nix/tarball/master") {}).hie86;

in

  {

    allowUnfree = true;

    packageOverrides = pkgs: with pkgs; {

      myPython = python3.withPackages (ps: with ps; [
        numpy
      ]);

      myVscode = pkgs.buildEnv {
        name = "myVscode";
        paths = [
          vscode
          _hie
        ];
      };

      myPackages = pkgs.buildEnv {
        name = "myPackages";
        paths = [

          bogofilter

          cabal2nix
          cabal-install
          chromium
          claws-mail
          cppcheck
          ctags

          evince

          ffmpeg

          gcc
          gdb
          geany
          ghc
          ghostwriter
          gimp
          gnome3.defaultIconTheme
          gnome3.eog
          gnumake

          heroku
          hexchat
          hlint

          imagemagick
          inkscape

          _julia

          lftp
          libreoffice

          man-pages
          _pkgs1809.meld

          nixops
          nodejs
          nodePackages.node2nix

          pcmanfm
          pdftk 
          python3Packages.glances
          python3Packages.pygments
          python3Packages.pylint

          qtcreator

          rustup

          simplescreenrecorder
          slack
          sqlite
          sqlitebrowser
          stack
          sylpheed

          texlive.combined.scheme-full
          tokei

          udiskie
          unzip

          valgrind
          _vim 
          vlc

          xfce.xfce4-screenshooter
          xfce.xfce4-terminal
          xorg.xkill

        ];
      };

    };
  }

