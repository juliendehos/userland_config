{ config, pkgs, ... }: {

  imports = [ ./hardware-configuration.nix ];

  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda"; 
  };

  environment.systemPackages = with pkgs; [
    arandr
    bash
    chromium
    cmake
    dmenu
    gnome3.eog
    evince
    firefox
    git
    ghc
    htop
    i3status
    libreoffice
    networkmanagerapplet
    nox 
    opencv3
    pavucontrol
    python3
    rxvt_unicode
    sudo
    sylpheed
    texlive.combined.scheme-full
    tmux
    tree
    vim 
    vlc
   ];

  hardware.pulseaudio.enable = true;

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "fr";
    defaultLocale = "fr_FR.UTF-8";
  };

  networking.hostName = "ty"; 
  networking.networkmanager.enable = true; 

  services = {

    printing = {
      enable = true;
      browsing = true;
      clientConf = "ServerName 193.49.192.5";
    };

    xserver = {
      enable = true;
      layout = "fr";
      #displayManager.sddm.enable = true;
      desktopManager.xfce.enable = true;
      #windowManager.i3.enable = true;
      synaptics.enable = true;
      #videoDrivers = [ "nvidia" ];
      #videoDrivers = [ "nvidiaLegacy340" ];
    };
  };

  system.stateVersion = "17.09";

  time.timeZone = "Europe/Paris";

  users.extraUsers.julien = {
    isNormalUser = true;
    uid = 1000;
    extraGroups = [ "wheel" "audio" "networkmanager" "vboxusers" ];
  };

  virtualisation.virtualbox.host.enable = true;
}

