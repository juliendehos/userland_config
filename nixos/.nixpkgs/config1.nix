# nix-env -iA nixos.myPackages

{
  packageOverrides = pkgs: with pkgs; rec {

    _vim = import ./vim.nix { inherit pkgs; };

    myPackages = pkgs.buildEnv {
      name = "my-packages";
      paths = [
        cabal2nix
        cabal-install
        chromium
        cmake
        ctags
        evince
        gcc
        geany
        ghc
        gimp
        gnome3.eog
        gnumake
        inkscape
        libreoffice
        htop
        nixops
        python3Packages.pygments
        sylpheed
        texlive.combined.scheme-full
        _vim
        vlc
      ];
    };
  };
}

