with (import <nixos> {});

let

  _julia = callPackage ./julia.nix {};

  _vim = import ./vim.nix { inherit pkgs; };

  # _vscode = vscode-with-extensions.override {
  #   vscodeExtensions = with vscode-extensions; [ ms-vscode.cpptools ];
  # };

in

{

  allowUnfree = true;  # vscode

  packageOverrides = pkgs: with pkgs; {

    #myPyEnv = python3.withPackages (ps: with ps; [ numpy ]);

    myPackages = pkgs.buildEnv {
      name = "myPackages";
      paths = [

        _julia

        python3Packages.tensorflow

        ranger
        binutils
        wget
        rustup

        patchelf
        xfce.xfce4-terminal
        gnome3.defaultIconTheme
        claws-mail

        # editeurs 
        geany
        _vim 
        ctags
        #universal-ctags 
        #clang libcxx libcxxStdenv 

        # linters
        hlint
        cppcheck
        #python3Packages.flake8
        #shellcheck
        #proselint
        #flow

        # haskell
        cabal2nix
        cabal-install
        ghc
        #stack

        # c/c++
        #cmake
        gcc
        gdb
        gnumake
        valgrind
        qtcreator

        # python 
        #python3 
        #python3Packages.virtualenv
        #python3Packages.pip
        #python3Packages.matplotlib
        #python3Packages.numpy

        # mail
        bogofilter
        sylpheed
        claws-mail

        slack

        # webpage
        filezilla
        lftp
        #marp
        #python3Packages.grip

        # latex
        python3Packages.pygments
        texlive.combined.scheme-full

        # divers dev
        tokei
        sqlitebrowser
        sqlite
        meld
        #julia_07
        #gnuplot
        #nodejs-8_x
        nodejs
        nodePackages.node2nix

        # nix/devops
        nixops
        heroku

        # divers systeme
        #apg 
        #baobab
        #binutils
        man-pages
        pcmanfm
        #pwgen 
        python3Packages.glances
        udiskie
        unzip
        xorg.xkill
        arandr

        # bureautique
        chromium
        evince
        ffmpeg
        gimp
        gnome3.eog
        inkscape
        libreoffice-fresh
        #mypaint hicolor_icon_theme
        pdftk 
        simplescreenrecorder
        vlc
        xfce.xfce4-screenshooter
        #xournal
        imagemagick

        #atom
        #_vscode
        vscode
        spotify
        #vscode-with-extensions
        #emacs
        hexchat

        pcmanfm

      ];
    };
  };

}


