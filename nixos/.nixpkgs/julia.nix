# copy this file in ~/.config/nixpkgs/julia.nix 
#
# then edit ~/.config/nixpkgs/config.nix like this:
#
# with (import <nixos> {});
# let
#   _julia = callPackage ./julia.nix {};
# in
# {
#   packageOverrides = pkgs: with pkgs; {
#     myPackages = pkgs.buildEnv {
#       name = "myPackages";
#       paths = [
#         _julia = callPackage ./julia.nix {};
#         ...

{ stdenv, makeWrapper, fetchurl }:

let
  version = "1.0.3";
  sha256 = "05vir1rmn73zcmn0dafmz4fbdhzzkkqh7q94z1j4lpfzs9ksharn";
  _pname = "julia-${version}";
in

stdenv.mkDerivation rec {
  inherit version;
  name = "julia-bin-${version}";

  src = fetchurl {
    url = "https://julialang-s3.julialang.org/bin/linux/x64/1.0/${_pname}-linux-x86_64.tar.gz";
    inherit sha256;
  };

  buildPhase = ":";

  installPhase = ''
    mkdir -p $out
    tar zxf $src -C $out
    mv $out/${_pname}/* $out/
    rmdir $out/${_pname}
  '';

  preFixup = ''
    patchelf --set-interpreter "$(cat $NIX_CC/nix-support/dynamic-linker)" $out/bin/julia
  '';
}

