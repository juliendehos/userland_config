{ pkgs, ... }:

{
  home.packages = [
    pkgs.geany
  ];

  programs.firefox = {
    enable = true;
  };
}

