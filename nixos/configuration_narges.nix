{ config, pkgs, ... }: {

  imports = [ ./hardware-configuration.nix ];
  
  nix.binaryCaches = [ 
    "https://cache.nixos.org/"
  ];

  #boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.kernelModules = [ "coretemp" ];

  boot.extraModprobeConfig = ''
    options snd_hda_intel enable=0
    options snd-usb-audio enable=1 index=1
    options snd slots=snd-usb-audio
  '';

  sound.enable = true;

  sound.extraConfig = ''
    defaults.ctl.card 1
    defaults.pcm.card 1
    defaults.timer.card 1
    pcm.monomic {
      type plug
      slave {
        pcm "sysdefault:1"
        channels 1
      }
      hint {
        description "Mono microphone"
      }
    }
  '';

  boot.loader.grub = {
    device = "/dev/sda";
    enable = true;
    extraEntries = ''
      menuentry 'Void Linux (ln)' {
        insmod gzio
        insmod part_gpt
        insmod ext2
        set root='hd0,gpt3'
        linux   /boot/ln-vmlinuz root=/dev/sda3 ro  quiet
        initrd  /boot/ln-initrd
      }
    '';
    version = 2;
  };

  environment.systemPackages = with pkgs; [
    bash
    dmenu
    firefox
    git
    i3status
    rxvt_unicode
    sudo
    tmux
    tree
    htop
    file
    vim
  ];

  fileSystems."/vm" = { 
    device = "/dev/disk/by-label/vm";
    fsType = "ext4";
  };

  fileSystems."/data" = { 
    device = "/dev/disk/by-label/data";
    fsType = "ext4";
  };

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "fr-bepo";
    defaultLocale = "fr_FR.UTF-8";
  };

  networking = {
    firewall.allowedTCPPortRanges = [ { from = 3037; to = 3042; } ];
    # firewall.allowedTCPPorts = [ 3000 ];
    #interfaces.eth0.ip4 = [ { address = "192.168.45.107"; prefixLength = 25; } ];
    #defaultGateway = "192.168.45.18";
    #nameservers = [ "193.49.195.10" "195.220.130.2" "193.49.195.20" "195.220.130.10" ];
    hostName = "narges"; 
  };

  services = {

    printing = {
      enable = true;
      browsing = true;
      clientConf = "ServerName 192.168.45.5";
    };
    
    dbus.enable = true;
    udisks2.enable = true;

    postgresql = {
      enable = true;
      package = pkgs.postgresql;
      enableTCPIP = true;
      authentication = ''
        local all all trust
      '';
    };

    openssh.enable = true;

    xserver = {
      enable = true;
      layout = "fr";
      displayManager.lightdm.enable = true;
      windowManager.i3.enable = true;
      xkbVariant = "bepo";
    };
  };

  system.stateVersion = "18.09";

  time.timeZone = "Europe/Paris";

  virtualisation.virtualbox.host.enable = true;
  virtualisation.docker.enable = true;

}

