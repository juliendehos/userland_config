
# https://www.reddit.com/r/NixOS/comments/9bb9h9/post_your_homemanager_homenix_file/

{ pkgs, ... }:

{

  imports = [
    ./git.nix
    ./packages.nix
    ./vim.nix
    ./vscode.nix
  ];

  nixpkgs.config.allowUnfree = true;

  #home.stateVersion = "20.09";

  home.keyboard = {
    layout = "fr,us(altgr-intl)";
    variant = "bepo,";
    options = [
      "grp:alt_shift_toggle"
      "caps:swapescape"
    ];
  };

  home.sessionVariables = {
    EDITOR = "vim";
  };

# opam configuration
# test -r /home/jd/.opam/opam-init/init.sh && . /home/jd/.opam/opam-init/init.sh > /dev/null 2> /dev/null || true
#

  gtk = {
    enable = true;
    iconTheme = {
      name = "Adwaita";
      package = pkgs.gnome3.adwaita-icon-theme;
    };
    theme = {
      name = "Shades-of-gray";
      package = pkgs.shades-of-gray-theme;
    };
  };

  qt = {
    enable = true;
    platformTheme = "gtk";
  };

  programs = {

    bash = {
      enable = true;
      shellAliases = {
        ll = "ls -lh";
        la = "ls -a";
        #covideo19 = "covideo19-record covideo19.herokuapp.com 80 1120 480 800 600 25";
      };

      sessionVariables = {
        # EDITOR = "vim";
        # PS1 = "\n\[\w]\$\[\033[0m\]";
      };
    };

    dircolors = {
      enable = true;
      settings.DIR = "01;33";
    };

    direnv = {
      enable = true;
      enableNixDirenvIntegration = true;
    };

    home-manager = {
      enable = true;
      path = "_";
    };

    firefox.enable = true;
    chromium.enable = true;

    #urxvt = {
    #  enable = true;
    #  fonts = ["xft:DejaVu Sans Mono:size=13"];
    #    extraConfig = {
    #      reverseVideo = true;
    #      termName = "xterm-256color";
    #    };
    #};

  };

  services = {

    #network-manager-applet.enable = true;

    #pasystray.enable = true;

  };

  #xsession.enable = true;

}

