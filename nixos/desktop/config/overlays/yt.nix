self: super: 

{

  yt = with super; python3Packages.buildPythonPackage rec {

    pname = "youtube-dl";
    # The websites youtube-dl deals with are a very moving target. That means that
    # downloads break constantly. Because of that, updates should always be backported
    # to the latest stable release.
    version = "2020.11.01.1";

    src = fetchurl {
      url = "https://yt-dl.org/downloads/${version}/${pname}-${version}.tar.gz";
      sha256 = "06lhba4b9bm6f5yqrb5xvdr0l5shwd95djf9nlpg86prr5xihqks";
    };

    nativeBuildInputs = [ installShellFiles makeWrapper ];
    buildInputs = [ zip ] ;
    #propagatedBuildInputs = lib.optional hlsEncryptedSupport pycryptodome;

    # Ensure these utilities are available in $PATH:
    # - ffmpeg: post-processing & transcoding support
    # - rtmpdump: download files over RTMP
    # - atomicparsley: embedding thumbnails
    makeWrapperArgs = let
      packagesToBinPath = [ atomicparsley ffmpeg rtmpdump phantomjs2 ];
    in [ ''--prefix PATH : "${lib.makeBinPath packagesToBinPath}"'' ];

    setupPyBuildFlags = [
      "build_lazy_extractors"
    ];

    postInstall = ''
      installShellCompletion youtube-dl.zsh
    '';

    # Requires network
    doCheck = false;
  };

}

