self: super: {

  kickstart = self.rustPlatform.buildRustPackage rec {
    pname = "kickstart";
    version = "v0.2.1";

    src = self.fetchFromGitHub {
      owner = "Keats";
      repo = pname;
      rev = version;
      sha256 = "0imrwkqgaafn2g94wqwfyi5ldi0h09kg5fq8fm9g0p3qc2xbfwr5";
    };

    doCheck = false;
    cargoSha256 = "1h859p693n8ap8jb06ym67jvysi8dap25wk4w1nl1111zyin543p";

  };

}

