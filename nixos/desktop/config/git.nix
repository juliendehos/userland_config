{ pkgs, ... }:

{

  programs.git = {
    enable = true;
    userName = "Julien Dehos";
    userEmail = "dehos@univ-littoral.fr";
    ignores =  [
      "*~"
      "*.swp"
    ];

    extraConfig = {
      credential = {
        helper = "cache --timeout=10800";
      };
    };

  };

}


