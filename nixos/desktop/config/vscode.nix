{ pkgs, ... }:

{

  programs.vscode = {
    enable = true;

      #haskell = {
      #   enable = true;
      #   hie = {
      #     enable = true;
      #      executablePath = "${pkgs.hies}/bin/hie-wrapper";
      #   };
      #};

      extensions = with pkgs.vscode-extensions; [

        bbenoist.Nix
        ms-vscode.cpptools
        ms-python.python
        #vscodevim.vim

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "nix-env-selector";
            publisher = "arrterian";
            version = "0.1.2";
            sha256 = "1n5ilw1k29km9b0yzfd32m8gvwa2xhh6156d4dys6l8sbfpp2cv9";
          };
        })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "cmake-tools";
            publisher = "ms-vscode";
            version = "1.4.2";
            sha256 = "1azjqd5w14q1h8z6cib4lwyk3h9hl1lzzrnc150inn0c7v195qcl";
          };
        })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "cmake";
            publisher = "twxs";
            version = "0.0.17";
            sha256 = "11hzjd0gxkq37689rrr2aszxng5l9fwpgs9nnglq3zhfa1msyn08";
          };
        })

        # (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        #   mktplcRef = {
        #     name = "ghcide";
        #     publisher = "DigitalAssetHoldingsLLC";
        #     version = "0.0.2";
        #     sha256 = "02gla0g11qcgd6sjvkiazzk3fq104b38skqrs6hvxcv2fzvm9zwf";
        #   };
        # })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "vscode-ghc-simple";
            publisher = "dramforever";
            version = "0.1.22";
            sha256 = "0x3csdn3pz5rhl9mhplpm8kxb40l1dw5rnwhh3zsif3rz0nqhk2a";
          };
        })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "language-haskell";
            publisher = "JustusAdam";
            version = "3.3.0";
            sha256 = "1285bs89d7hqn8h8jyxww7712070zw2ccrgy6aswd39arscniffs";
          };
        })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "restructuredtext";
            publisher = "lextudio";
            version = "129.0.0";
            sha256 = "01n7xbcb9nzwrcc0wphb530cgjg0681f02ryry9xqbr1r7b2lwxx";
          };
        })

        (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
          mktplcRef = {
            name = "todo-tree";
            publisher = "Gruntfuggly";
            version = "0.0.186";
            sha256 = "0frnfimvv2862bb85jgyvbky90xpxx584mir9k2fsgii1rxmv6dr";
          };
        })

      ];

      # userSettings = {

      #   # general settings
      #   "window.zoomLevel" = 1.25;
      #   "editor.fontSize" = 13;
      #   "keyboard.dispatch" = "keyCode";
      #   "update.enableWindowsBackgroundUpdates" = false;
      #   "update.mode" = "none";

      #   # # vim
      #   # "vim.easymotion" = true;
      #   # "vim.leader" = ",";

      #   "cmake.configureOnOpen" = false;

      #   #"code-runner.runInTerminal" = true;
      #   
      #   # python
      #   #"python.linting.pylintPath" = "~/.nix-profile/bin/pylint";

      #   # restructuredtext
      #   #"restructuredtext.linter.executablePath" = "~/.nix-profile/bin/doc8";
      #   #"restructuredtext.sphinxBuildPath" = "~/.nix-profile/bin/sphinx-build";
      #   #"restructuredtext.preview.sphinx.disabled" = true;
      #   

      #   # "window.titleBarStyle" = "custom";
      #   #"languageServerHaskell.hieExecutablePath" = "hie-wrapper";
      #   #"languageServerHaskell.hlintOn" = true;
      #   # "languageServerHaskell.enableHIE" = true;
      #   # "languageServerHaskell.trace.server" = "verbose";
      #   # "update.channel" = "none";
      #   # "[nix]"."editor.tabSize" = 2;

      # };

    };

    # home.file.".config/Code/User/keybindings.json".text = ''
    #   [
    #     {
    #         "key": "ctrl+`",
    #         "command": "workbench.action.terminal.focus"
    #     },
    #     {
    #         "key": "ctrl+`",
    #         "command": "workbench.action.focusActiveEditorGroup",
    #         "when": "terminalFocus"
    #     }
    #   ]
    # '';


  }


