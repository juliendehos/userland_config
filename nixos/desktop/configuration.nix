# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];

  # boot.kernelPackages = pkgs.linuxPackages_4_19;

  virtualisation.docker.enable = true;
  virtualisation.virtualbox.host.enable = true;

  networking.hostName = "nixos";
  time.timeZone = "Europe/Paris";
  system.stateVersion = "20.09";

  #programs.ssh.forwardX11 = true;

  environment.systemPackages = with pkgs; [

    cudatoolkit
    cachix
    git
    htop

    pavucontrol
    pulseaudioFull
    #jack2Full

    tmux
    tree
    vim
    wget

  ];

  nix.extraOptions = ''
    keep-outputs = true
    keep-derivations = true
  '';

  boot.loader.grub = {
    device = "/dev/sda";
    enable = true;
    extraEntries = ''
      menuentry 'librazik bl' {
        insmod gzio
        insmod part_gpt
        insmod ext2
        set root='hd0,gpt5'
        linux   /boot/vmlinuz-bl root=/dev/sda5 rw quiet
        initrd  /boot/initrd-bl
      }
      menuentry 'librazik' {
        insmod gzio
        insmod part_gpt
        insmod ext2
        set root='hd0,gpt5'
        linux   /boot/vmlinuz root=/dev/sda5 rw quiet
        initrd  /boot/initrd
      }
    '';
    version = 2;
  };

  nix.trustedUsers = [
    "julien"
    "root"
  ];

  boot.blacklistedKernelModules = [ "snd_hda_intel" ];
  hardware.pulseaudio.enable = true;
  sound.enable = true;

  # boot.extraModprobeConfig = ''
  #   options snd_hda_intel enable=0
  #   options snd-usb-audio enable=1 index=2
  #   options snd slots=snd-usb-audio
  # '';
  # sound.extraConfig = ''
  #   defaults.ctl.card 2
  #   defaults.pcm.card 2
  #   defaults.timer.card 2
  #   pcm.monomic {
  #     type plug
  #     slave {
  #       pcm "sysdefault:2"
  #       channels 1
  #     }
  #     hint {
  #       description "Mono microphone"
  #     }
  #   }
  # '';

  fileSystems."/data4" = { 
    device = "/dev/disk/by-label/data4";
    fsType = "ext4";
  };

  console = {
    font = "Lat2-Terminus16";
    keyMap = "fr-bepo";
  };

  i18n.defaultLocale = "fr_FR.UTF-8";

  services = {

    vnstat.enable = true;

    flatpak.enable = true;

    dbus = {
      enable = true;
      packages = [ pkgs.gnome3.dconf ];
    };

    #postgresql = {
    #  enable = true;
    #  package = pkgs.postgresql;
    #  authentication = "local all all trust";
    #};

    #jack.jackd = { enable = true; };

    xserver = {
      enable = true;
      layout = "fr,us(altgr-intl)";
      xkbVariant = "bepo,";
      xkbOptions = "grp:alt_shift_toggle";
      displayManager.lightdm.enable = true;
      desktopManager.xfce.enable = true;

      ##videoDrivers = [ "nvidia_legacy_390" ];
      videoDrivers = [ "nvidia" ];
    };

  };

  # # nix-direnv
  # # nix options for derivations to persist garbage collection
  # nix.extraOptions = ''
  #   keep-outputs = true
  #   keep-derivations = true
  # '';
  # environment.pathsToLink = [
  #   "/share/nix-direnv"
  # ];

  nixpkgs.config.allowUnfree = true;

  xdg.portal = {
    enable = true;
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };

}

