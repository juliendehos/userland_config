# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{

  imports = [ ./hardware-configuration.nix ];

  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda";
    extraEntries = ''
      menuentry 'Debian GNU/Linux' --class debian --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-simple-01dc9c9f-57a3-4e55-af74-d4368d2c07e 1' {
        insmod gzio
	if [ x$grub_platform = xxen ]; then insmod xzio; insmod lzopio; fi
        insmod part_gpt
        insmod ext2
        set root='hd0,gpt7'
        if [ x$feature_platform_search_hint = xy ]; then
          search --no-floppy --fs-uuid --set=root --hint-bios=hd0,gpt7 --hint-efi=hd0,gpt7 --hint-baremetal=ahci0,gpt7  01dc9c9f-57a3-4e55-af74-d4368d2c07e1
        else
          search --no-floppy --fs-uuid --set=root 01dc9c9f-57a3-4e55-af74-d4368d2c07e1
        fi
        linux   /boot/vmlinuz-3.16.0-4-amd64 root=UUID=01dc9c9f-57a3-4e55-af74-d4368d2c07e1 ro  quiet
        initrd  /boot/initrd.img-3.16.0-4-amd64
      }
      menuentry "FreeBSD" {
        set root=(hd0,gpt3)
        kfreebsd /boot/loader
      }
    '';
  };

  fileSystems."/data" = { 
    device = "/dev/disk/by-label/data";
    fsType = "ext4";
  };

  virtualisation.virtualbox.host.enable = true;

  hardware.pulseaudio.enable = true;

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "fr-bepo";
    defaultLocale = "fr_FR.UTF-8";
  };

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    rxvt_unicode
    bash
    vim
    i3status
    dmenu
    git
  ];

  nixpkgs.config.allowUnfree = true; 

  networking.hostName = "narges"; 

  time.timeZone = "Europe/Paris";

  services = {

    openssh.enable = true;

    printing = {
      enable = true;
      browsing = true;
      clientConf = ''
        ServerName 193.49.192.5
      '';
    };

    xserver = {
      enable = true;
      #videoDrivers = [ "ati_unfree" ];
      windowManager.i3.enable = true;
      layout = "fr";
      xkbVariant = "bepo";
      #xkbOptions = "eurosign:e";
    };

  };

  system.stateVersion = "16.03";

}
