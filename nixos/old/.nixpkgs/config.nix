pkgs : {

  packageOverrides = pkgs : with pkgs; rec {

    myVim = vim_configurable.customize {

      name = "myVim";

      vimrcConfig.customRC = ''
        set nocompatible

        " Leader - ( Spacebar ) "
        let mapleader = " "

        " display options "
        set t_Co=256
        set gfn=Monospace\ 13
        syntax on
        colorscheme molokai
        let g:molokai_original = 1
        let g:rehash256 = 1

        set number
        set cursorline
        set colorcolumn=80
        set ruler         " show the cursor position all the time "
        set showcmd       " display incomplete command "

        " search "
        set hlsearch
        set incsearch     " do incremental searching "
        set ignorecase    " case insensitive searching (unless specified) "
        set smartcase

        " backup and undo "
        set undofile
        set history=100

        " completion "
        set wildmode=longest:full,full
        set wildmenu

        " mouse and scrolling "
        set mouse=a
        set scrolloff=8 

        " tabulation "
        set tabstop=4
        set expandtab
        set shiftwidth=4

        " Open new split panes to right and bottom, which feels more natural "
        set splitbelow
        set splitright

        " HTML Editing "
        set matchpairs+=<:>

        " Quicker window movement "
        nnoremap <C-j> <C-w>j
        nnoremap <C-k> <C-w>k
        nnoremap <C-h> <C-w>h
        nnoremap <C-l> <C-w>l

        " Always use vertical diffs "
        set diffopt+=vertical

        " resize panes "
        nnoremap <silent> <Right> :vertical resize +5<cr>
        nnoremap <silent> <Left> :vertical resize -5<cr>
        nnoremap <silent> <Up> :resize +5<cr>
        nnoremap <silent> <Down> :resize -5<cr>

        " navigate through compilation errors "
        nmap <F4> :cp<cr>
        nmap <F5> :cn<cr>

        nnoremap <C-c> :bp\|bd #<CR>

        " -- plugins -- "
        filetype off

        " airline "
        set laststatus=2

        " nerdtree "
        nmap <F6> :NERDTreeToggle<CR>

        " tagbar "
        nmap <F7> :TagbarToggle<CR>

        " vim-buffergator "
        nmap <F2> gb
        nmap <F3> gB

        " youcompleteme "
        let g:ycm_global_ycm_extra_conf = '~/.nixpkgs/.ycm_extra_conf.py'

        filetype plugin indent on
        " -- plugins -- "
         '';

      vimrcConfig.vam.knownPlugins = pkgs.vimPlugins; 

      vimrcConfig.vam.pluginDictionaries = [
        { name = "airline"; }
        { name = "easymotion"; }
        { name = "molokai"; }
        { name = "nerdtree"; }
        { name = "tagbar"; }
        { name = "vim-buffergator"; }
        { name = "youcompleteme"; }
      ];
    };

    myAll = pkgs.buildEnv {
      name = "myAll";
      paths = [ 
        ctags
        myVim
      ];
    };

  };

}
