{ config, pkgs, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda"; 
  };

  hardware.pulseaudio.enable = true;

  networking.hostName = "ty"; 
  networking.networkmanager.enable = true; 

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "fr";
    defaultLocale = "fr_FR.UTF-8";
  };

  time.timeZone = "Europe/Paris";

  environment.systemPackages = with pkgs; [
    arandr
    bash
    chromium
    cmake
    dmenu
    gnome3.eog
    evince
    firefox
    git
    ghc
    htop
    i3status
    libreoffice
    networkmanagerapplet
    nox 
    opencv3
    pavucontrol
    python3
    rxvt_unicode
    sudo
    sylpheed
    texlive.combined.scheme-full
    tmux
    vim 
    vlc
   ];

  services = {
    xserver = {
      enable = true;
      layout = "fr";
      displayManager.sddm.enable = true;
      desktopManager.xfce.enable = true;
      #windowManager.i3.enable = true;
    };

    printing = {
      enable = true;
      browsing = true;
      clientConf = "ServerName 193.49.192.5";
    };
  };

  users.extraUsers.julien = {
    isNormalUser = true;
    uid = 1000;
    extraGroups = [ "wheel" "audio" "networkmanager" ];
  };

  system.stateVersion = "17.03";

}
