# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{

  imports = [ ./hardware-configuration.nix ];

  boot.extraModprobeConfig = ''
    options snd slots=snd-usb-audio
  '';

  hardware.pulseaudio.enable = true;
  #hardware.pulseaudio = {
  #  enable = true;
  #  package = pkgs.pulseaudioFull;
  #};

  boot.loader.grub = {
    enable = true;
    version = 2;
    device = "/dev/sda";
    extraEntries = ''
      menuentry 'Void Linux (ln)' {
        insmod gzio
        insmod part_gpt
        insmod ext2
        set root='hd0,gpt3'
        linux   /boot/ln-vmlinuz root=/dev/sda3 ro  quiet
        initrd  /boot/ln-initrd
      }
      menuentry 'Librazik' {
        insmod gzio
        insmod part_gpt
        insmod ext2
        set root='hd0,gpt5'
        linux   /boot/vmlinuz-4.6.0-1-lzk-bl-amd64 root=/dev/sda5 ro  quiet
        initrd  /boot/initrd.img-4.6.0-1-lzk-bl-amd64 
      }
      menuentry 'Librazik (ln)' {
        insmod gzio
        insmod part_gpt
        insmod ext2
        set root='hd0,gpt5'
        linux   /boot/ln-vmlinuz root=/dev/sda5 ro  quiet
        initrd  /boot/ln-initrd
      }
    '';
  };

  fileSystems."/data" = { 
    device = "/dev/disk/by-label/data";
    fsType = "ext4";
  };

  #virtualisation.virtualbox.host.enable = true;


  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "fr-bepo";
    defaultLocale = "fr_FR.UTF-8";
  };

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    rxvt_unicode
    bash
    vim
    i3status
    dmenu
    git
    pavucontrol
    sudo
    nox
  ];

  #nixpkgs.config.allowUnfree = true; 

  networking.hostName = "z14"; 

  time.timeZone = "Europe/Paris";

  services = {
    xserver = {
      enable = true;
      #videoDrivers = [ "ati_unfree" ];
      windowManager.i3.enable = true;
      layout = "fr";
      xkbVariant = "bepo";
      #xkbOptions = "eurosign:e";
    };
  };

  users.extraUsers.dehos = { 
    isNormalUser = true;
    home = "/home/dehos";
    extraGroups = [ "wheel" "audio" ];
  };

  # The NixOS release to be compatible with for stateful data such as databases.
  system.stateVersion = "16.09";

}



