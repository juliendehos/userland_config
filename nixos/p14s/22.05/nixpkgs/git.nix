{ pkgs, ... }:

{

  programs.git = {
    enable = true;
    package = pkgs.gitAndTools.gitFull;
    userName = "Julien Dehos";
    userEmail = "dehos@univ-littoral.fr";

    ignores =  [
      ".*~"
      "*.swp"
      ".envrc"
      ".direnv"
      "__pycache__"
      "*.egg-info"
      #".vscode"
      "build*"
      "dist-*"
    ];

    extraConfig = {
      init.defaultBranch = "main";
      pull.rebase = false;
      credential.helper = "cache --timeout=10800";
    };

  };

}

