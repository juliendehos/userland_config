self: super: 

let 
  tpmgr-src = self.fetchFromGitLab {
    owner = "juliendehos";
    repo = "tpmgr";
    rev = "v0.4";
    sha256 = "sha256-GWJuOLByGOrCgyokY4njcASQx0WJssY+tTyDr4bOMvc=";
  };

in {
  tpmgr = self.callPackage "${tpmgr-src}/default.nix" {};
}

