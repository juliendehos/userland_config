{ pkgs, ... }:

{

  programs.vscode = {
    enable = true;

    extensions = with pkgs.vscode-extensions; [

      bbenoist.nix
      ms-vscode.cpptools

      ms-python.python
      vscodevim.vim

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "epub-reader";
          publisher = "cweijan";
          version = "1.0.0";
          sha256 = "sha256-wundHVZ0GNcddIh1Af+fBobdKsswk+MMoFVnI7hjgTQ=";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "pdf";
          publisher = "tomoki1207";
          version = "1.2.0";
          sha256 = "sha256-/2u2yfAmSVBvfXIMt2VokkeYH8q8Y9Ca9YSDuwwpkq0=";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "idris-vscode";
          publisher = "meraymond";
          version = "0.0.13";
          sha256 = "sha256-UViP9Wadb12H4UdpULp8TGwJqCGWryjWs9ugUmZwn8A=";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "agda-mode";
          publisher = "banacorn";
          version = "0.3.9";
          sha256 = "sha256-qzpAtEDcAwE6viqW1sHc0HtW6gEtymE/gX112mhNF0c=";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "nix-env-selector";
          publisher = "arrterian";
          version = "1.0.8";
          sha256 = "sha256-GH9kl81yrOxxAPzP4hdLpGvU9x79gjqictF7sx4tj8k=";
          #sha256 = "sha256:11hzjd0gxkq37689rrr2aszxng5l9fwpgs9nnglq3zhfa1msyn09";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "cmake-tools";
          publisher = "ms-vscode";
          version = "1.12.12";
          sha256 = "sha256-ixs1YOxXWNkhFFW5luJxqzgKcs4ZbkxlECnjWNeYFk8=";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "cmake";
          publisher = "twxs";
          version = "0.0.17";
          sha256 = "sha256:11hzjd0gxkq37689rrr2aszxng5l9fwpgs9nnglq3zhfa1msyn08";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "haskell";
          publisher = "haskell";
          version = "2.2.0";
          sha256 = "sha256-YGPytmI4PgH6GQuWaRF5quiKGoOabkv7On+WVupI92E=";
        };
      })

      # (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
      #   mktplcRef = {
      #     name = "vscode-ghc-simple";
      #     publisher = "dramforever";
      #     version = "0.2.3";
      #     sha256 = "sha256:1pd7p4xdvcgmp8m9aymw0ymja1qxvds7ikgm4jil7ffnzl17n6kp";
      #   };
      # })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "language-haskell";
          publisher = "JustusAdam";
          version = "3.6.0";
          sha256 = "sha256-rZXRzPmu7IYmyRWANtpJp3wp0r/RwB7eGHEJa7hBvoQ=";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "restructuredtext";
          publisher = "lextudio";
          version = "190.1.4";
          sha256 = "sha256-u7uXzAeAFqOFcdAEOCZlTYIApRCo9VkXC0t2E6JYRfg=";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "todo-tree";
          publisher = "Gruntfuggly";
          version = "0.0.215";
          sha256 = "sha256-WK9J6TvmMCLoqeKWh5FVp1mNAXPWVmRvi/iFuLWMylM=";
        };
      })

    ];
  };


      #   # general settings
      #   "window.zoomLevel" = 1.25;
      #   "editor.fontSize" = 13;
      #   "keyboard.dispatch" = "keyCode";
      #   "update.enableWindowsBackgroundUpdates" = false;
      #   "update.mode" = "none";

      #   # # vim
      #   # "vim.easymotion" = true;
      #   # "vim.leader" = ",";

      #   "cmake.configureOnOpen" = false;

      #   #"code-runner.runInTerminal" = true;
      #   
      #   # python
      #   #"python.linting.pylintPath" = "~/.nix-profile/bin/pylint";

      #   # restructuredtext
      #   #"restructuredtext.linter.executablePath" = "~/.nix-profile/bin/doc8";
      #   #"restructuredtext.sphinxBuildPath" = "~/.nix-profile/bin/sphinx-build";
      #   #"restructuredtext.preview.sphinx.disabled" = true;
      #   

      #   # "window.titleBarStyle" = "custom";
      #   #"languageServerHaskell.hieExecutablePath" = "hie-wrapper";
      #   #"languageServerHaskell.hlintOn" = true;
      #   # "languageServerHaskell.enableHIE" = true;
      #   # "languageServerHaskell.trace.server" = "verbose";
      #   # "update.channel" = "none";
      #   # "[nix]"."editor.tabSize" = 2;

      # };

    # home.file.".config/Code/User/keybindings.json".text = ''
    #   [
    #     {
    #         "key": "ctrl+`",
    #         "command": "workbench.action.terminal.focus"
    #     },
    #     {
    #         "key": "ctrl+`",
    #         "command": "workbench.action.focusActiveEditorGroup",
    #         "when": "terminalFocus"
    #     }
    #   ]
    # '';


  }


