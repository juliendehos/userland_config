{ config, pkgs, ... }:

{
  # Let Home Manager install and manage itself.
  #programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "test";
  home.homeDirectory = "/home/test";


  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "23.05";


  imports = [
    #./spacemacs.nix
    #./emacs.nix
    # ./doom-emacs.nix

    ./git.nix
    ./i3status.nix
    ./i3.nix
    ./packages.nix
    ./vim.nix
    ./nvim.nix
    ./vscode.nix
    # ./mail.nix

  ];

  nixpkgs.overlays = [ 
    (import ./overlays/kickstart.nix)
    (import ./overlays/tpmgr.nix)
    (import ./overlays/vimPlugins.nix)
  ];

  #nixpkgs.config.allowUnfree = true;

  home = {
    sessionVariables = {
      EDITOR = "vim";
    };
    #keyboard = {
    #  options = [ "caps:swapescape" ];
    #};

  };
  
  home.keyboard = {
    layout = "us(altgr-intl)";
    #layout = "us(altgr-intl),fr";
    #variant = ",bepo";
    #options = [
    #  #"grp:alt_shift_toggle"
    #  "caps:swapescape"
    #];
  };

  fonts.fontconfig.enable = true;

  home.packages = [
    (pkgs.nerdfonts.override { 
      fonts = [
        "DejaVuSansMono"
      ]; 
    })
  ];

  #home.file.".stack/config.yaml".text = ''
  #  nix:
  #      enable: true
  #      pure: false
  #      packages: [zlib]
  #'';

  gtk = {
    enable = true;
    iconTheme = {
      name = "Adwaita";
      package = pkgs.gnome3.adwaita-icon-theme;
    };
    theme = {
      name = "Shades-of-gray";
      package = pkgs.shades-of-gray-theme;
    };
  };

  qt = {
    enable = true;
    platformTheme = "gtk";
  };

  programs = {

    bash = {
      enable = true;
      shellAliases = {
        ll = "ls -lh";
        la = "ls -a";
      #  vbox = "VirtualBox -style Fusion";
      };

      #sessionVariables = {
      #  # TODO PS1 = "\n\[\w]\$\[\033[0m\]";
      #};
    };

    dircolors = {
      enable = true;
      settings.DIR = "01;33";
    };

    direnv = {
      enable = true;
      nix-direnv.enable = true;
    };

    home-manager = {
      enable = true;
      # path = "_";
    };

    firefox.enable = true;

    chromium.enable = true;

    obs-studio = {
      enable = true;
    };

    #urxvt = {
    #  enable = true;
    #  fonts = ["xft:DejaVu Sans Mono:size=13"];
    #    extraConfig = {
    #      reverseVideo = true;
    #      termName = "xterm-256color";
    #    };
    #};

  };

  services = {

    network-manager-applet.enable = true;

    pasystray.enable = true;

    #xscreensaver = {
    #  enable = true;
    #  settings = {
    #     fade = false;
    #     lock = false;
    #     # mode = "blank";
    #     mode = "one";
    #     selected = 0;
    #     timeout = "0:05:00";

    #     programs = "bsod -root -delay 60 -no-windows -no-nt -no-2k -no-win10 -no-msdos -no-amiga -no-glados -no-android -no-apple2 -no-ransomware -no-nvidia -no-os2 -no-mac -no-mac1 -no-vmware -no-macsbug -no-macx -no-os390 -no-vms -no-hvx -no-blitdamage -no-atm -no-sparclinux -no-hppalinux -no-solaris -no-tru64";

    #  };
    #};

    udiskie = {
      enable = true;
      tray = "always";
    };

  };

  #xsession = {
  #  enable = true;
  #  profileExtra = ''
  #    xrandr --output eDP --brightness 0.5
  #    setxkbmap -option "caps:swapescape"
  #  '';
  #};

  #xdg.mimeApps = {
  #  enable = true;
  #  associations.added = {
  #    "application/pdf" = ["org.gnome.Evince.desktop"];
  #  };
  #  defaultApplications = {
  #    "application/pdf" = ["org.gnome.Evince.desktop"];
  #  };
  #};


}

