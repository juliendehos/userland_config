
self: super: 

let 
  myproject-src = self.fetchFromGitLab {
    owner = "nokomprendo";
    repo = "nokomprendo.gitlab.io";
    rev = "0bb6f2e";
    sha256 = "sha256-4F4GXnMVWrE5BzJcewopr6xGmdPySeawwbvN66Cu9b0=";
  };

in {
  myproject = self.callPackage "${myproject-src}/posts/tuto_093/code/myproject-h4n/default.nix" {};
}

