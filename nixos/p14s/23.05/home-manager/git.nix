{ pkgs, ... }:

{

  programs.git = {
    enable = true;
    package = pkgs.gitAndTools.gitFull;
    userName = "Julien Dehos";
    userEmail = "dehos@univ-littoral.fr";

    ignores =  [
      ".*~"
      "*.swp"
      ".envrc"
      "__pycache__"
      "*.egg-info"
      "build*"
      "dist-*"
      ".direnv"
    ];

    extraConfig = {
      init.defaultBranch = "main";
      pull.rebase = false;
      credential.helper = "cache --timeout=10800";
      http.postBuffer  = "2097152000";
      https.postBuffer  = "2097152000";
    };

  };

}

