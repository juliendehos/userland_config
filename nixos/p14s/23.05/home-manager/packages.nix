
{ pkgs, ... }:

  {

  home.packages = with pkgs; [

    #gpxlab
    #josm
    viking
    gpsprune
    #zombietrackergps

    mplayer
    xclip
    #neovide
    deluge

    #blender
    calibre
    #pandoc
    sigil
#    obsidian
    #drive
    appimage-run


    dune_2
    ocaml

    #kickstart
    cookiecutter
    doxygen

    #lean
    why3
    #exercism
    #catch2
    #chez
    #racket
    z3
    # coq
    idris2
    (agda.withPackages (p: [ p.standard-library ]))


    #termonad-with-packages
    lxterminal
    xfce.xfce4-terminal
    alacritty


    baobab

    #llvm_9

    bristol
    ams-lv2
    avldrums-lv2
    string-machine
    zita-at1
    vcv-rack
    gnome.zenity
    yoshimi
    zynaddsubfx
    samplv1
    reaper
    musescore

    #plantuml
    gifsicle


    drumkv1
    surge

    adlplug
    xfce.mousepad
    ardour
    drumgizmo
    guitarix
    helm
    hydrogen
    lingot
    #bitwig-studio

    ffmpeg
    vlc
    guvcview
    kdenlive
    #blender

    #haskell-language-server
    (haskellPackages.ghcWithPackages (ps: with ps; [
      aeson
      fourmolu
      ormolu
      mtl
      text
      transformers
      megaparsec
      QuickCheck
      random
      rio
    ]))

    tpmgr

    #(import (fetchTarball "https://gitlab.com/juliendehos/runcpp/-/archive/master/runcpp-master.tar.gz") {})

    #(import (fetchTarball "https://gitlab.com/juliendehos/autoexam/-/archive/v0.4/autoexam-v0.4.tar.gz") {})
    # (import (fetchTarball "https://gitlab.com/juliendehos/autoexam/-/archive/master/autoexam-master.tar.gz") {})

    #(import (fetchTarball "https://gitlab.com/juliendehos/autoquizer/-/archive/v0.4/autoquizer-v0.4.tar.gz") {})
    # (import (fetchTarball "https://gitlab.com/juliendehos/autoquizer/-/archive/master/autoquizer-master.tar.gz") {})

    audacity
    acpi
    arandr
    aspellDicts.fr

    #bat
    #bc
    #binutils-unwrapped

    cabal-install
    cmake
    #cmakeWithGui
    #cppcheck
    #ctags

    evince

    ffmpeg

    gcc
    #gdb
    geany
    gimp
    gitg
    gnome3.eog
    gnumake
    gnuplot
    gparted
    guvcview
    glances
    
    #heroku
    hlint
    hunspell
    hunspellDicts.fr-any
    hunspellDicts.fr-classique

    imagemagick
    inkscape

    #julia-stable

    #klavaro
    killall
    krita

    libreoffice
    librsvg

    meld
    mypaint

    #networkmanagerapplet
    #nim
    #nodejs
    #nodePackages.node2nix

    opam
    #obs-studio

    pulseaudio
    pcmanfm
    #python2Packages.doc8    # restructuredtext_lint-1.2.2 not supported for interpreter python3.7

    (python3.withPackages (ps: with ps; [
     pygments
     sphinx
     pybind11
    #   sphinx_rtd_theme

    #   (buildPythonPackage rec {
    #     pname = "rstcheck";
    #     version = "3.3.1";
    #     src = pkgs.fetchFromGitHub {
    #       owner = "myint";
    #       repo = "${pname}";
    #       rev = "v${version}";
    #       sha256 = "0wl5mlc7b8sifn5s3c5wv0ga1b99xf7ni6ig186dabpywhv48270";
    #     };
    #     doCheck = false;
    #     propagatedBuildInputs = [ docutils ];
    #   })

    ]))

    ranger
    #rustup

    simplescreenrecorder
    sqlite
    sqlitebrowser
    #stack
    #slack
    #spotify

    tokei
    #texlive.combined.scheme-full
    thunderbird
    tor-browser-bundle-bin

    #udiskie
    unar
    unzip

    valgrind
    vlc

    #xfce.xfce4-battery-plugin
    xfce.xfce4-screenshooter
    #xfce.terminal
    xorg.xkill
    xournal

    #youtube-dl
    yt-dlp

    zlib
    #zoom-us
    #zotero

  ];

}

