# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      <musnix>
    ];

  boot.kernelPackages = pkgs.linuxPackages_latest_rt;
  #virtualisation.virtualbox.host.enable = true;
  
  musnix.enable = true;
  sound.enable = true;
  hardware.pulseaudio.enable = true;
  # hardware.pulseaudio.package = pkgs.pulseaudio.override { jackaudioSupport = true; };

  services.jack = {
    jackd.enable = true;
    alsa.enable = false;
    loopback = {
      enable = true;
      # buffering parameters for dmix device to work with ALSA only semi-professional sound programs
      #dmixConfig = ''
      #  period_size 2048
      #'';
    };
  };

  programs.light.enable = true;

  nix.trustedUsers = [ "test" "root" ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking = {
    hostName = "nixos"; # Define your hostname.
    networkmanager.enable = true;
  };

  time.timeZone = "Europe/Paris";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  #networking.useDHCP = false;
  #networking.interfaces.enp2s0f0.useDHCP = true;
  #networking.interfaces.enp5s0.useDHCP = true;
  #networking.interfaces.wlp3s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  xdg.portal.enable = true;

  virtualisation.docker.enable = true;

  services = {
    #acpid.enable = true;

    fstrim.enable = true;

    dbus.packages = [ pkgs.gnome3.dconf ];
    dbus.enable = true;

    avahi = {
      enable = true;
      nssmdns = true;
      #publish.userServices = true;
      #publish.enable = true;
    };


    xserver = {
      enable = true;
      layout = "us";
      windowManager.i3.enable = true;
      displayManager.lightdm.enable = true;
      wacom.enable = true;
      videoDrivers = [ "amdgpu" ];
      xkbVariant = "altgr-intl";
      xkbOptions = "caps:swapescape";
      libinput.enable = true;
    };
  };

  
  # nix-direnv
  nix.extraOptions = ''
    keep-outputs = true
    keep-derivations = true
  '';


  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.test = {
    isNormalUser = true;
    extraGroups = [ 
      "audio" 
      "docker" 
      "networkmanager" 
      "video" 
      "wheel" 
    ];
  };

  users.users.toto = {
    isNormalUser = true;
    extraGroups = [ 
      "audio" 
      "jackaudio" 
      "networkmanager" 
      "video" 
    ];
  };

  environment.systemPackages = with pkgs; [
    wget 
    vim
    git
    glances
    htop
    pavucontrol
    firefox
    cachix
    qjackctl 
    file 
    tree 
    tmux

    jack2Full 
    pulseaudioFull
    jack_capture
    obs-studio
    obs-v4l2sink
    obs-linuxbrowser
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.09"; # Did you read the comment?

}

