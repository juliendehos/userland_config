{ pkgs, ... }:

{

  programs.git = {
    enable = true;
    package = pkgs.gitAndTools.gitFull;
    userName = "Julien Dehos";
    userEmail = "dehos@univ-littoral.fr";

    ignores =  [
      "*~"
      "*.swp"
    ];

    extraConfig = {
      pull.rebase = false;
      credential.helper = "cache --timeout=10800";
    };

  };

}

