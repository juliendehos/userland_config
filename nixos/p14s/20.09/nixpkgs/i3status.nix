{ pkgs, config, ... }:

{
  
  programs.i3status = {

    enable = true;
    enableDefault = false;

    general = {
      output_format = "i3bar";
      colors = true;
      color_good = "#80d060";
      color_bad = "#d06060";
      interval = 5;
    };

    modules = {

      "wireless wlp3s0" = {
        position = 1;
        settings = {
          format_up = " W: (%quality at %essid) %ip ";
          format_down = " W: down ";
        };
      };

      "ethernet enp5s0" = {
        position = 2;
        settings = {
          format_up = " E1: %ip (%speed) ";
          format_down = " E1: down ";
        };
      };

      "ethernet enp2s0f0" = {
        position = 3;
        settings = {
          format_up = " E2: %ip (%speed) ";
          format_down = " E2: down ";
        };
      };

      "battery 0" = {
        position = 4;
        settings = {
          format = " B: %status %percentage %remaining (%consumption) ";
          low_threshold = 10;
          threshold_type = "percentage";
        };
      };

      "volume master" = {
        position = 5;
        settings = {
          format = " ♪: %volume ";
          device = "pulse:0";
          # device = "default"
          # mixer = "Master"
          # mixer_idx = 0
        };
      };

      "load" = {
        position = 6;
        settings = {
          format = " L: %5min ";
        };
      };

      "cpu_usage" = {
        position = 7;
        settings = {
          format = " CPU: %usage ";
        };
      };

      "cpu_temperature 0" = {
        position = 8;
        settings = {
          format = " T: %degrees C";
          max_threshold = 75;
          #path = "/sys/devices/virtual/thermal/thermal_zone0/temp";
          path = "/sys/devices/platform/thinkpad_hwmon/hwmon/hwmon3/temp1_input";
        };
      };

      "disk /" = {
        position = 9;
        settings = {
          format = " /: %avail ";
        };
      };

      "tztime local" = {
        position = 10;
        settings = {
          format = " %Y-%m-%d %H:%M ";
        };
      };

      #"volume master" = {
      #  position = 1;
      #  settings = {
      #    format = "♪ %volume";
      #    format_muted = "♪ muted (%volume)";
      #    device = "pulse:1";
      #  };
      #};

      #"disk /" = {
      #  position = 2;
      #  settings = {
      #    format = "/ %avail";
      #  };
      #};

    };

  };

}

