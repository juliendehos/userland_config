{ pkgs, ... }:

  {

  home.packages = with pkgs; [
    gnome3.adwaita-icon-theme
    breeze-icons

    bristol
    ams-lv2
    avldrums-lv2
    string-machine
    zita-at1
    #vcv-rack
    yoshimi
    zynaddsubfx
    samplv1


    opentoonz
    #synfigstudio
    drumkv1
    surge

    adlplug
    xfce.mousepad
    ardour
    drumgizmo
    guitarix
    helm
    hydrogen
    lingot

    youtube-dl
    ffmpeg
    vlc
    guvcview
    obs-studio
    kdenlive
    blender

    godot

    kickstart

    (haskellPackages.ghcWithPackages (ps: with ps; [
      aeson
      random
      mtl
      text
      transformers
    ]))

    #(import (fetchTarball "https://gitlab.com/juliendehos/runcpp/-/archive/master/runcpp-master.tar.gz") {})

    #(import (fetchTarball "https://gitlab.com/juliendehos/autoexam/-/archive/v0.4/autoexam-v0.4.tar.gz") {})
    # (import (fetchTarball "https://gitlab.com/juliendehos/autoexam/-/archive/master/autoexam-master.tar.gz") {})

    #(import (fetchTarball "https://gitlab.com/juliendehos/autoquizer/-/archive/v0.4/autoquizer-v0.4.tar.gz") {})
    # (import (fetchTarball "https://gitlab.com/juliendehos/autoquizer/-/archive/master/autoquizer-master.tar.gz") {})

    audacity
    acpi
    arandr
    aspellDicts.fr

    #bat
    bc
    #binutils-unwrapped

    cabal-install
    cachix
    #chez
    cmakeWithGui
    #cppcheck
    ctags

    #discord

    #electron
    evince

    ffmpeg

    gcc
    gdb
    geany
    gimp
    gitg
    #gnome3.cheese
    gnome3.eog
    #gnome3.gnome-sound-recorder
    gnumake
    gnuplot
    gparted
    guvcview
    glances
    
    heroku
    hlint
    hunspellDicts.fr-any

    imagemagick
    inkscape

    #julia

    klavaro
    killall
    krita

    libreoffice
    librsvg

    meld
    mypaint

    networkmanagerapplet
    nim
    #nodejs
    #nodePackages.node2nix

    opam
    obs-studio

    pcmanfm
    #python2Packages.doc8    # restructuredtext_lint-1.2.2 not supported for interpreter python3.7

    (python3.withPackages (ps: with ps; [
     pygments
     sphinx
    #   sphinx_rtd_theme

    #   (buildPythonPackage rec {
    #     pname = "rstcheck";
    #     version = "3.3.1";
    #     src = pkgs.fetchFromGitHub {
    #       owner = "myint";
    #       repo = "${pname}";
    #       rev = "v${version}";
    #       sha256 = "0wl5mlc7b8sifn5s3c5wv0ga1b99xf7ni6ig186dabpywhv48270";
    #     };
    #     doCheck = false;
    #     propagatedBuildInputs = [ docutils ];
    #   })

    ]))

    ranger
    rustup

    simplescreenrecorder
    sqlite
    sqlitebrowser
    #stack

    tokei
    texlive.combined.scheme-full
    thunderbird

    udiskie
    unar
    unzip

    valgrind
    vlc

    xfce.xfce4-battery-plugin
    xfce.xfce4-screenshooter
    xfce.terminal
    xorg.xkill
    xournal

    youtube-dl

    zlib
    zoom-us

  ];

}

