{ pkgs, config, ... }:

{

  xsession.windowManager.i3 = {

    enable = true;

    extraConfig = "exec i3-msg workspace 1";

    config = let mod = "Mod4"; in {

      fonts = [ "DejaVu Sans 12" ];

      bars = [{
        fonts = [ "DejaVu Sans 12" ]; 
        statusCommand = "${pkgs.i3status}/bin/i3status"; 
      }];

      modifier = mod;

      workspaceLayout = "tabbed";

      keybindings = pkgs.lib.mkOptionDefault {

        "${mod}+m" = "exec ${pkgs.i3lock}/bin/i3lock -n -c 000000";

        "${mod}+c" = "split h";

        "${mod}+h" = "focus left";
        "${mod}+j" = "focus down";
        "${mod}+k" = "focus up";
        "${mod}+l" = "focus right";

        "${mod}+Shift+h" = "move left";
        "${mod}+Shift+j" = "move down";
        "${mod}+Shift+k" = "move up";
        "${mod}+Shift+l" = "move right";

        "${mod}+Shift+e" = "exec ~/.config/nixpkgs/i3_exit_menu.sh";

        "XF86MonBrightnessUp" = "exec light -A 10";
        "XF86MonBrightnessDown" = "exec light -U 10";

        "XF86AudioRaiseVolume" = "exec pactl set-sink-volume 0 +5%";
        "XF86AudioLowerVolume" = "exec pactl set-sink-volume 0 -5%";
        "XF86AudioMute" = "exec pactl set-sink-mute 0 toggle";
      };

    };

  };

}

