{ config, pkgs, ... }:

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "test";
  home.homeDirectory = "/home/test";

home.packages = with pkgs; [
    htop
    glances
    ardour
    helm
    drumgizmo
    lingot
    guitarix
    xfce.xfce4-battery-plugin
    qjackctl
youtube-dl
ffmpeg
vlc
guvcview
obs-studio
  ];


  programs.chromium = {
    enable = true;
};

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.03";
}
