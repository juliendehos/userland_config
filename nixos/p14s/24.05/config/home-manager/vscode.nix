{ pkgs, ... }:

{

  programs.vscode = {
    enable = true;

    extensions = with pkgs.vscode-extensions; [

      bbenoist.nix
      arrterian.nix-env-selector
      mhutchie.git-graph
      gruntfuggly.todo-tree
      # vscodevim.vim

      ms-vscode.cpptools
      ms-vscode.cmake-tools
      twxs.cmake

      ms-python.python

      banacorn.agda-mode

      # haskell.haskell
      justusadam.language-haskell

      rust-lang.rust-analyzer

      # ocamllabs.ocaml-platform

      # (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
      #   mktplcRef = {
      #     name = "vscode-ghc-simple";
      #     publisher = "dramforever";
      #     version = "0.2.3";
      #     sha256 = "sha256:1pd7p4xdvcgmp8m9aymw0ymja1qxvds7ikgm4jil7ffnzl17n6kp";
      #   };
      # })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "lean4";
          publisher = "leanprover";
          version = "0.0.159";
          sha256 = "sha256-P7Fe7HVpFB79Y9QtHrAS2sdnHTa3cv5uxSmHFiKfXMY";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "idris2-lsp";
          publisher = "bamboo";
          version = "0.7.0";
          sha256 = "sha256-8eLvHKUPBoge50wzOfp5aK/XVJElVzKtil8Yj+PwNUU=";
        };
      })

      (pkgs.vscode-utils.buildVscodeMarketplaceExtension {
        mktplcRef = {
          name = "vscode-ghc-simple";
          publisher = "dramforever";
          version = "0.2.4";
          sha256 = "sha256-CMECeHvUCCpJG2gUk9FvbUF8T2+04ngWDLgPMsR1FjQ=";
        };
      })

    ];
  };
}

