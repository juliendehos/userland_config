
; (require 'package)
; (package-initialize 'noactivate)
; (eval-when-compile
;   (require 'use-package))
; 
; (use-package 
;   flycheck
;   :defer 2
;   :config (global-flycheck-mode))

(setq inhibit-startup-message t
      inhibit-startup-echo-area-message (user-login-name))

;(setq initial-major-mode 'fundamental-mode
;      initial-scratch-message nil)

(load-theme 'gruvbox t)

(load-file (let ((coding-system-for-read 'utf-8))
                (shell-command-to-string "agda-mode locate")))

(set-face-attribute 'default nil :height 130)

