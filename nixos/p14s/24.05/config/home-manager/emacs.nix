{ pkgs, ... }:

{

  programs.emacs = {

    enable = true;

    extraPackages = epkgs: (with epkgs.melpaStablePackages; [
      #agda2-mode
      gruvbox-theme
      #company
      #counsel
      #flycheck
      #ivy
      #magit
      #projectile
      #use-package
    ]);

    # extraConfig = ''
    #   (require 'package)
    #   (package-initialize 'noactivate)
    #   (eval-when-compile
    #     (require 'use-package))
    #   (use-package 
    #     flycheck
    #     :defer 2
    #     :config (global-flycheck-mode))
    # '';

  };

  home.file = {
    ".emacs.d" = {
      source = ./emacs;
      recursive = true;
    };
  };

}

