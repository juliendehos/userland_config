# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      <musnix>
    ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  boot.kernelPackages = pkgs.linuxPackages_latest;
  #boot.kernelPackages = pkgs.linuxPackages_latest_rt;

  virtualisation.virtualbox.host.enable = true;
  virtualisation.docker.enable = true;
  
  musnix.enable = true;
  #sound.enable = true;
  #hardware.pulseaudio.enable = true;
  # hardware.pulseaudio.package = pkgs.pulseaudio.override { jackaudioSupport = true; };
  #services.jack = {
  #  jackd.enable = true;
  #  alsa.enable = false;
  #  loopback = {
  #    enable = true;
  #    # buffering parameters for dmix device to work with ALSA only semi-professional sound programs
  #    #dmixConfig = ''
  #    #  period_size 2048
  #    #'';
  #  };
  #};

  #programs.light.enable = true;

  nix.trustedUsers = [ "test" "root" ];

  networking = {
    hostName = "nixos"; # Define your hostname.
    networkmanager.enable = true;
  };

  time.timeZone = "Europe/Paris";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  #networking.useDHCP = false;
  #networking.interfaces.enp2s0f0.useDHCP = true;
  #networking.interfaces.enp5s0.useDHCP = true;
  #networking.interfaces.wlp3s0.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "fr_FR.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  xdg.portal.enable = true;
  
  programs.dconf.enable = true;

  services = {
    #acpid.enable = true;

    fstrim.enable = true;

    dbus.packages = [ 
      pkgs.gnome3.dconf 
      #pkgs.gnome2.GConf 
    ];
    dbus.enable = true;

    #avahi = {
    #  enable = true;
    #  nssmdns = true;
    #  #publish.userServices = true;
    #  #publish.enable = true;
    #};

    #vnstat.enable = true;

    
    interception-tools = {
      enable = true;
      plugins = [ pkgs.interception-tools-plugins.caps2esc ];
    };

    
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      #jack.enable = true;

      # use the example session manager (no others are packaged yet so this is enabled by default,
      # no need to redefine it in your config for now)
      #media-session.enable = true;
    };


    xserver = {
      enable = true;
      layout = "us";
      xkbVariant = "altgr-intl";
      #xkbOptions = "caps:swapescape";
      libinput.enable = true;
      displayManager.autoLogin.enable = true;
      displayManager.autoLogin.user = "test";
      displayManager.lightdm.enable = true;
      windowManager.i3.enable = true;
      videoDrivers = [ "amdgpu" ];
      wacom.enable = true;
    };
  };

  security.rtkit.enable = true; # for pipewire

  # nix-direnv
  nix.extraOptions = ''
    keep-outputs = true
    keep-derivations = true
  '';
  environment.pathsToLink = [
    "/share/nix-direnv"
  ];


  powerManagement = {
    enable = true;
    powertop.enable = true;
  };


  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users = {

    test = {
      isNormalUser = true;
      extraGroups = [ 
        "audio" 
        "docker" 
        "networkmanager" 
        "video" 
        "wheel" 
      ];
    };

    toto = {
      isNormalUser = true;
      extraGroups = [ 
        "audio" 
        "jackaudio" 
        "networkmanager" 
        "video" 
      ];
    };

  };

  environment.systemPackages = with pkgs; [
    brightnessctl
    wget 
    vim
    git
    glances
    htop
    pavucontrol
    #firefox
    cachix
    #qjackctl 
    file 
    tree 
    tmux

    alsaUtils
    #alsaPlugins
    #jack2Full 
    #pulseaudioFull
    #jack_capture
    #obs-studio
    #obs-v4l2sink
    #obs-linuxbrowser
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

}

