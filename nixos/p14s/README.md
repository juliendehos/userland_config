
## nixos

https://nixos.org/manual/nixos/stable/


## musnix

https://github.com/musnix/musnix

nix-channel --add https://github.com/musnix/musnix/archive/master.tar.gz musnix
nix-channel --update musnix


## home-manager

https://github.com/nix-community/home-manager

nix-channel --add https://github.com/nix-community/home-manager/archive/release-20.09.tar.gz home-manager
nix-channel --update

nix-shell '<home-manager>' -A install




cryptsetup luksFormat /dev/disk/by-uuid/3f6b0024-3a44-4fde-a43a-767b872abe5d
cryptsetup luksOpen /dev/disk/by-uuid/3f6b0024-3a44-4fde-a43a-767b872abe5d crypted
mkfs.ext4 /dev/mapper/crypted

boot.initrd.luks.devices.crypted.device = "/dev/disk/by-uuid/3f6b0024-3a44-4fde-a43a-767b872abe5d";
fileSystems."/".device = "/dev/mapper/crypted";

boot.loader.grub.enableCryptodisk = true;
