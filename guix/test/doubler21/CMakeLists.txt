cmake_minimum_required( VERSION 2.6 )
project( doubler21 )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" )
add_executable( doubler21 src/doubler21.cpp src/Doubler.cpp )
install (TARGETS doubler21 DESTINATION bin)

