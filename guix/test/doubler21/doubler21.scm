(define-module (doubler21)
  #:use-module (guix gexp) 
  #:use-module (guix packages)
  #:use-module (gnu packages cmake)
  #:use-module (guix build-system cmake)
  #:use-module (guix licenses))

(define-public doubler21
  (package
    (name "doubler21")
    (version "0.1")
    (source (local-file "" #:recursive? #t))
    (build-system cmake-build-system)
    (arguments '(#:tests? #f)) 
    (synopsis "doubler21")
    (description "Doubler 21.")
    (home-page "http://www.example.org")
    (license bsd-3)))

