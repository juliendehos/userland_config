(define-module (test_hello)
  #:use-module (guix gexp) 
  #:use-module (guix packages)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses))

(define-public test_hello
  (package
    (name "test_hello")
    (version "0.2")
    (source (local-file "" #:recursive? #t))
    (build-system gnu-build-system)
    (arguments '(#:phases 
                 (modify-phases %standard-phases (delete 'configure))))
    (synopsis "hello world test")
    (description "Hello world test.")
    (home-page "http://www.example.org")
    (license gpl3+)))

