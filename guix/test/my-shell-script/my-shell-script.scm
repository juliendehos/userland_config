(define-module (my-shell-script)
  #:use-module (guix gexp) 
  #:use-module (guix packages) 
  #:use-module (guix build-system trivial)
  #:use-module (guix licenses))

(define-public my-shell-script
  (package
    (name "my-shell-script")
    (version "0.1")
    ;(source (local-file (dirname (current-filename)) #:recursive? #t))
    (source (local-file "my-shell-script.sh"))
    (build-system trivial-build-system)
    (arguments
      `(#:modules ((guix build utils))
        #:builder
        (begin
          (use-modules (guix build utils))
          (let* ((bin-dir  (string-append %output "/bin"))
                 (bin-file (string-append bin-dir "/my-shell-script" )))
            (mkdir-p bin-dir)
            (copy-file (assoc-ref %build-inputs "source") bin-file)
            (chmod bin-file #o555)))))
    (home-page #f)
    (synopsis "bla bla")
    (description "More verbose bla bla")
    (license #f)))


