(define-module (test_arma)
  #:use-module (guix gexp) 
  #:use-module (guix packages)
  #:use-module (gnu packages maths)
  #:use-module (guix build-system gnu)
  #:use-module (guix licenses))

(define-public test_arma
  (package
    (name "test_arma")
    (version "0.2")
    (source (local-file "" #:recursive? #t))
    (build-system gnu-build-system)
    (arguments '(#:phases 
                 (modify-phases %standard-phases (delete 'configure))))
    (inputs `(("armadillo" ,armadillo)))
    (synopsis "armadillo test")
    (description "armadillo test.")
    (home-page "http://www.example.org")
    (license gpl3+)))

