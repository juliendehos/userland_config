(define-module (bmx_cv)
  #:use-module (guix gexp) 
  #:use-module (guix packages) 
  #:use-module (gnu packages cmake) 
  #:use-module (gnu packages pkg-config)
  #:use-module (opencv) 
  #:use-module (guix build-system cmake)
  #:use-module (guix licenses))

(define-public bmx_cv
  (package
    (name "bmx_cv")
    (version "0.1")
    (source (local-file "" #:recursive? #t))
    (build-system cmake-build-system)
    (native-inputs `(("pkg-config" ,pkg-config)))
    (inputs `(("opencv" ,opencv)))
    (arguments '(#:tests? #f)) 
    (synopsis "bmx_cv")
    (description "bmx_cv")
    (home-page "http://www.example.org")
    (license bsd-3)))

