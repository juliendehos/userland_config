(define-module (opencv31)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages video)
  #:use-module (gnu packages tbb)
  #:use-module (gnu packages zip)
  #:use-module (guix build-system cmake)
  #:use-module (guix licenses))

(define-public opencv31
  (package
    (name "opencv31")
    (version "3.1.0")
    (source 
      (origin
        (method url-fetch)
        (uri (string-append 
               "https://github.com/opencv/opencv/archive/" version ".zip"))
        (sha256
          (base32
            "1912wrsb6nfl9fp7w9z3n0x04jcrv6k6zsa0zx7q10nvkwj90s8z"))))
    (build-system cmake-build-system)
    (arguments '(#:configure-flags '("-DWITH_IPP=OFF")
                 ;#:parallel-build? #f
                 #:tests? #f)) 
    (native-inputs
      `(("pkg-config" ,pkg-config)
        ("cmake" ,cmake)
        ("unzip" ,unzip)))
    (inputs
      `(("atlas" ,atlas)
        ("doxygen" ,doxygen)
        ("eigen" ,eigen)
        ("ffmpeg" ,ffmpeg)
        ("gst-libav" ,gst-libav)
        ("gst-plugins-base" ,gst-plugins-base)
        ;("gst-plugins-good" ,gst-plugins-good)
        ;("gst-plugins-bad" ,gst-plugins-bad)
        ("gstreamer" ,gstreamer)
        ("gtk+" ,gtk+)
        ;("lapack" ,lapack)  ; problem with lapacke
        ;("blas" ,openblas)
        ("python" ,python)
        ("tbb" ,tbb)))
    (synopsis 
      "open source computer vision and machine learning software library")
    (description 
      "OpenCV (Open Source Computer Vision Library) is an open source computer
      vision and machine learning software library. OpenCV was built to provide
      a common infrastructure for computer vision applications and to
      accelerate the use of machine perception in the commercial products.")
    (home-page "http://opencv.org")
    (license bsd-3)))

