#include <opencv2/opencv.hpp>
#include <iostream>

int main(int argc, char ** argv) {

    std::cout << "OpenCV: " << cv::getBuildInformation();

    if (argc != 2) {
        std::cerr << "usage: " << argv[0] << " <video>" << std::endl;
        exit(-1);
    }

    // init input files
    const std::string videoFile = argv[1];
    cv::VideoCapture cap(videoFile); 
    if(not cap.isOpened()) {
        std::cerr << "error: unable to open capture device" << std::endl;
        exit(-1);
    }

    // init display
    cv::namedWindow(videoFile);
    bool isPlaying = true;

    // process video
    cv::UMat imgVideo;
    while (true) {

        // get frame
        if (isPlaying) {
            cap >> imgVideo; 
            if (imgVideo.empty()) 
                break;
        }

        // display frame and highlight template (if found)
        cv::imshow(videoFile, imgVideo);

        // handle time and keyboard
        int k = cv::waitKey(30); 
        if (k % 0x100 == 27)
            break; // quit if "esc"
        if (k % 0x100 == 32) 
            isPlaying = not isPlaying; // play/pause if "space"
    }

    return 0;
}

