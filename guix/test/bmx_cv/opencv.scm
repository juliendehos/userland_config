(define-module (opencv)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (gnu packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages video)
  #:use-module (gnu packages tbb)
  #:use-module (gnu packages zip)
  #:use-module (guix build-system cmake)
  #:use-module (guix licenses))

(define-public opencv
  (package
    (name "opencv")
    (version "3.2.0")
    (source 
      (origin
        (method url-fetch)
        (uri (string-append 
               "https://github.com/opencv/opencv/archive/" version ".zip"))
        (sha256
          (base32
            "0p0ppp3p7xnn4ah9g3a9nh7wav2jg2zq3mz1vnd50bk6aknhq06j"))))
    (build-system cmake-build-system)
    (arguments '(#:configure-flags '("-DWITH_IPP=OFF")
                 ;#:parallel-build? #f
                 #:tests? #f)) 
    (native-inputs
      `(("pkg-config" ,pkg-config)
        ("cmake" ,cmake)
        ("unzip" ,unzip)))
    (inputs
      `(("atlas" ,atlas)
        ("doxygen" ,doxygen)
        ("eigen" ,eigen)
        ("ffmpeg" ,ffmpeg)
        ("gst-libav" ,gst-libav)
        ("gst-plugins-base" ,gst-plugins-base)
        ;("gst-plugins-good" ,gst-plugins-good)
        ;("gst-plugins-bad" ,gst-plugins-bad)
        ("gstreamer" ,gstreamer)
        ("gtk+" ,gtk+)
        ;("lapack" ,lapack)  ; problem with lapacke
        ;("blas" ,openblas)
        ("python" ,python)
        ("tbb" ,tbb)))
    (synopsis 
      "open source computer vision and machine learning software library")
    (description 
      "OpenCV (Open Source Computer Vision Library) is an open source computer
      vision and machine learning software library. OpenCV was built to provide
      a common infrastructure for computer vision applications and to
      accelerate the use of machine perception in the commercial products.")
    (home-page "http://opencv.org")
    (license bsd-3)))

