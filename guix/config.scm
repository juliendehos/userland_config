;; This is an operating system configuration template
;; for a "desktop" setup without full-blown desktop
;; environments.

(use-modules (gnu) (gnu system nss) (gnu system grub))
(use-service-modules desktop)
(use-package-modules xorg fonts wm suckless xdisorg version-control certs gnuzilla vim)

(operating-system
  (host-name "z14")
  (timezone "Europe/Paris")
  (locale "fr_FR.UTF-8")

  ;; Assuming /dev/sdX is the target hard disk, and "my-root"
  ;; is the label of the target root file system.
  (bootloader (grub-configuration 
		(device "/dev/sda")
		(menu-entries 
		  (list (menu-entry
			   (label "voidlinux")
			   (linux "/boot/vmlinuz-4.9.11_1")
			   (linux-arguments '("root=/dev/sda3"))
			   (initrd "/boot/initramfs-4.9.11_1.img"))
			 (menu-entry
			   (label "voidlinux ln")
			   (linux "/boot/ln-vmlinuz")
			   (linux-arguments '("root=/dev/sda3"))
			   (initrd "/boot/ln-initramfs.img"))))))

  (file-systems (cons* (file-system
			 (device "guixsd-root")
			 (title 'label)
			 (mount-point "/")
			 (type "ext4"))
		       (file-system
			 (device "data")
			 (title 'label)
			 (mount-point "/data")
			 (type "ext4"))
		       %base-file-systems))

  (swap-devices '("/dev/sda2"))

  (users (cons (user-account
                (name "dehos")
                (group "users")
                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video"))
                (home-directory "/home/dehos"))
               %base-user-accounts))

  ;; Add a bunch of window managers; we can choose one at
  ;; the log-in screen with F1.
  (packages (cons* i3-wm i3status dmenu 
		   rxvt-unicode git icecat vim-full
                   nss-certs
                   %base-packages))

  ;; Use the "desktop" services, which include the X11
  ;; log-in service, networking with Wicd, and more.
  (services (cons* (console-keymap-service "fr")
	    %desktop-services))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
