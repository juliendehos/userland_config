
curl --remote-name --time-cond cacert.pem https://curl.haxx.se/ca/cacert.pem

guix archive --authorize < hydra.gnu.org.pub


## reconfiguration

```
sudo -E guix system reconfigure /etc/config.scm
```


## chercher un paquet

```
guix package -s nom_paquet | recsel -p name,version
```

