#!/bin/sh

REPOS=`find . -maxdepth 1 -mindepth 1 -type d`

git config --global credential.helper 'cache --timeout=60'

for REPO in $REPOS ; do
  echo ""
  echo "---- $REPO ----"
  cd $REPO
  git pull
  cd ..
done

