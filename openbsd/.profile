# $OpenBSD: dot.profile,v 1.4 2005/02/16 06:56:57 matthieu Exp $
#
# sh/ksh initialization

PATH=$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin:/usr/games:.
export PATH HOME TERM

export PKG_PATH=ftp://ftp.fr.openbsd.org/pub/OpenBSD/5.7/packages/amd64/

export LANG="fr_FR.UTF-8"
#export LANG="fr_FR.ISO8859-15"

alias stack=/home/dehos/.cabal/bin/stack

